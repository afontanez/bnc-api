package com.test.hbola.bnc.newservices;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.hbola.bnc.newconfig.TestSerivceConfiguration;


@RunWith(SpringJUnit4ClassRunner.class)   
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes={TestSerivceConfiguration.class})
public class ServiceFacadeImplTest {

//	 @Autowired
//	 private ServicesFacadeI servicesFacade;

	@Test
	public void getDataForModelCreation() {
	
		//HBODeocderModelCreationDTO dto = servicesFacade.getDataForModelCreation();	//this can't login unless tomcat is running - so no bnc calls can be tested
		assert(true); //but i want some test running in case there eventual is something that can be tested - this will serve as a working example
	}
}