package com.hbola.bnc.newutils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hbola.bnc.dto.HBOIrdDTO;
import com.hbola.bnc.dto.HBOServiceDTO;
import com.hbola.bnc.dto.HBOTierDTO;
import com.hbola.bnc.newconfig.TestSerivceConfiguration;
import com.hbola.bnc.utils.StringUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes = { TestSerivceConfiguration.class })
public class StringUtilsTest {

	@Test
	public void tripIRD() throws Exception {

		String xml = null;
		try {
			xml = StringUtils.setupDSR74xxDSR4470XMLTripRequest(null, 1);
		} catch (Exception e) {
			assertEquals("Could not create trip message. Unit address is null or empty", e.getMessage());
		}

		try {
			xml = StringUtils.setupDSR74xxDSR4470XMLTripRequest("", 1);
		} catch (Exception e) {
			assertEquals("Could not create trip message. Unit address is null or empty", e.getMessage());
		}

		xml = StringUtils.setupDSR74xxDSR4470XMLTripRequest("0001142040115", -7);
		assertNotNull(xml);
		assertTrue(!xml.isEmpty());
		assertTrue(xml.contains("<Message UtcTime="));// check random parts of
														// the xml
		assertTrue(xml.contains("<Request TransId=\"111.222.3.444-0\""));
		assertTrue(xml.contains("<setUnitConfig unitAddress=\"0001142040115\"/>"));

		xml = StringUtils.setupDSR74xxDSR4470XMLTripRequest("0001142040115", 1);
		assertNotNull(xml);
		assertTrue(!xml.isEmpty());
		assertTrue(xml.contains("<Message UtcTime="));// check random parts of
														// the xml
		assertTrue(xml.contains("<Request TransId=\"111.222.3.444-1\""));
		assertTrue(xml.contains("<setUnitConfig unitAddress=\"0001142040115\"/>"));
	}

	@Test
	public void validateServices() throws Exception {

		List<HBOServiceDTO> list = new ArrayList<HBOServiceDTO>();

		// test - make sure there is a service
		try {
			StringUtils.validateServices(null);
		} catch (Exception e) {
			assertEquals("No Services Setup For This Decoder", e.getMessage());
		}

		// test - make sure there is a service
		try {
			StringUtils.validateServices(list);
		} catch (Exception e) {
			assertEquals("No Services Setup For This Decoder", e.getMessage());
		}

		// test - make sure there is a service
		HBOServiceDTO dto = new HBOServiceDTO();
		list.add(dto);
		try {
			StringUtils.validateServices(list);
		} catch (Exception e) {
			assertEquals("Transcoder 1 : vch number must not be null and must be numeric", e.getMessage());
		}

		// test - happy path
		list.clear();
		dto.setHdEnable(true);
		dto.setOperation("Connection de Senal");
		dto.setPassthruEnable(true);
		dto.setSdEnable(true);
		dto.setTranscoderID(1);
		dto.setVcNumber("2410");
		dto.setVcTable("106");
		list.add(dto);
		StringUtils.validateServices(list);// everything will pass so no
											// exception thrown

		// test - make sure service has valid vc number
		list.clear();
		dto.setVcNumber(null);
		list.add(dto);
		try {
			StringUtils.validateServices(list);
		} catch (Exception e) {
			assertEquals("Transcoder 1 : vch number must not be null and must be numeric", e.getMessage());
		}

		// test - make sure service has valid vc table
		list.clear();
		dto.setVcNumber("2410");
		list.add(dto);
		dto.setVcTable(null);
		try {
			StringUtils.validateServices(list);
		} catch (Exception e) {
			assertEquals("Transcoder 1 : vc table must not be null and must be numeric", e.getMessage());
		}

		// test - make sure that 2 services are validated correctly
		list.clear();
		dto.setVcTable("106");
		HBOServiceDTO dto1 = new HBOServiceDTO();
		dto1.setHdEnable(true);
		dto1.setOperation("Connection de Senal");
		dto1.setPassthruEnable(true);
		dto1.setSdEnable(true);
		dto1.setTranscoderID(2);
		dto1.setVcNumber(null);
		dto1.setVcTable("106");
		list.add(dto);
		list.add(dto1);
		try {
			StringUtils.validateServices(list);
		} catch (Exception e) {
			assertEquals("Transcoder 2 : vch number must not be null and must be numeric", e.getMessage());
		}

		// test - make sure that middle service is validated correctly
		list.clear();
		HBOServiceDTO dto2 = new HBOServiceDTO();
		dto2.setHdEnable(true);
		dto2.setOperation("Connection de Senal");
		dto2.setPassthruEnable(true);
		dto2.setSdEnable(true);
		dto2.setTranscoderID(2);
		dto2.setVcNumber("2420");
		dto2.setVcTable("106");
		list.add(dto);
		list.add(dto2);
		list.add(dto1);
		try {
			StringUtils.validateServices(list);
		} catch (Exception e) {
			assertEquals("Transcoder 3 : vch number must not be null and must be numeric", e.getMessage());
		}

	}

	@Test
	public void extractMessageFromBNCResponse() {
		String value = StringUtils.extractMessageFromBNCResponse(null);
		assertEquals("", value);

		value = StringUtils.extractMessageFromBNCResponse("");
		assertEquals("", value);

		value = StringUtils.extractMessageFromBNCResponse("!@$!@$ASFGAGSAG");
		assertEquals("", value);

		value = StringUtils.extractMessageFromBNCResponse(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Message UtcTime=\"2017-12-08T18:48:30.526Z\" Version=\"1.0\"><Response Class=\"1 - Failure\" TransId=\"111.222.3.444-3\"><setUnitConfig responseInfo=\"Invalid Affilite and/or Location:HBOLA:HBOLA\"/></Response></Message>");
		assertEquals("\"Invalid Affilite and/or Location:HBOLA:HBOLA\"", value);

		value = StringUtils.extractMessageFromBNCResponse(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Message UtcTime=\"2017-12-18T22:42:04.215Z\" Version=\"1.0\"><Response Class=\"2 - Failure (XML parsing):java.lang.NumberFormatException: For input string: &quot;null&quot;\"/></Message>");
		assertEquals(" (XML parsing):java.lang.NumberFormatException: For input string: \"null\"\"", value);

		value = StringUtils.extractMessageFromBNCResponse(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Message UtcTime=\"2018-02-16T19:32:09.063Z\" Version=\"1.0\"><Response Class=\"2 - failure\" TransId=\"111.222.3.444-6\"><setUnitConfig responseInfo=\"Exception Type: BNCEjbStatusException&#xa;Why: {[26-6-27]   The unit address (000-11111-11111) is not in a valid address range.}&#xa;Where: BNCEjbStatusException&#xa;\"/></Response></Message>");
		assertEquals(
				"\"Exception Type: BNCEjbStatusException\nWhy: {[26-6-27]   The unit address (000-11111-11111) is not in a valid address range.}\nWhere: BNCEjbStatusException\n\"",
				value);
	}

	@Test
	public void validateAndFormat() {

		try {
			StringUtils.validateAndFormat(null, null);
		} catch (Exception e) {
			assertEquals("<missing label> cannot be null or empty", e.getMessage());
		}

		try {
			StringUtils.validateAndFormat("", null);
		} catch (Exception e) {
			assertEquals("<missing label> cannot be null or empty", e.getMessage());
		}

		try {
			StringUtils.validateAndFormat(null, "");
		} catch (Exception e) {
			assertEquals("<missing label> cannot be null or empty", e.getMessage());
		}

		try {
			StringUtils.validateAndFormat("", "");
		} catch (Exception e) {
			assertEquals("<missing label> cannot be null or empty", e.getMessage());
		}

		try {
			StringUtils.validateAndFormat("label1", null);
		} catch (Exception e) {
			assertEquals("label1 cannot be null or empty", e.getMessage());
		}

		try {
			String value = StringUtils.validateAndFormat(null, "value1");
			assertEquals("value1", value);
		} catch (Exception e) {
			assertTrue(false);// we can't be here
		}

		try {
			String value = StringUtils.validateAndFormat("label1", "value1");
			assertEquals("value1", value);
		} catch (Exception e) {
			assertTrue(false);// we can't be here
		}

		try {
			String value = StringUtils.validateAndFormat("label-1", "value-1");
			assertEquals("value1", value);
		} catch (Exception e) {
			assertTrue(false);// we can't be here
		}

		try {
			String value = StringUtils.validateAndFormat("!@$FSDGA", "Q@ASDFGGg");
			assertEquals("Q@ASDFGGg", value);
		} catch (Exception e) {
			assertTrue(false);// we can't be here
		}

		try {
			String value = StringUtils.validateAndFormat("label-1", "000-11111-22222-333");
			assertEquals("0001111122222", value);
		} catch (Exception e) {
			assertTrue(false);// we can't be here
		}

	}

	@Test
	public void setupDSR7412XMLRequest() {

		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		HBOIrdDTO dto = gson.fromJson(JSON_REQUEST_EXAMPLE, HBOIrdDTO.class);

		String xml = StringUtils.setupDSR74xxDSR4470XMLRequest(null, 1);
		assertNotNull(xml);
		assertEquals(xml, "");

		xml = StringUtils.setupDSR74xxDSR4470XMLRequest(null, null);
		assertNotNull(xml);
		assertEquals(xml, "");

		xml = StringUtils.setupDSR74xxDSR4470XMLRequest(dto, 1);
		assertNotNull(xml);

		assertTrue(xml.contains(
				"<service hdEnable=\"true\" passthruEnable=\"true\" sdEnable=\"false\" transcoderID=\"2\" vcNumber=\"2420\" vcTable=\"106\"/>"));// just
																																					// some
																																					// random
																																					// text
																																					// from
																																					// the
																																					// xml
																																					// or
																																					// we
																																					// can
																																					// just
																																					// test
																																					// the
																																					// entire
																																					// xml?

		dto.setModelNumber("DSR4470");
		xml = StringUtils.setupDSR74xxDSR4470XMLRequest(dto, 1);
		assertTrue(!xml.contains("peerUAList"));

		dto.setModelNumber("DSR7412");
		xml = StringUtils.setupDSR74xxDSR4470XMLRequest(dto, 1);
		assertTrue(xml.contains("peerUAList"));

		// HBOIrdDTO dto2 = gson.fromJson(JSON_REQUEST_EXAMPLE,
		// HBOIrdDTO.class);
		dto.getServices().get(0).setOperation("delete");
		xml = StringUtils.setupDSR74xxDSR4470XMLRequest(dto, 2);
		System.out.println("xml: " + xml);

	}

	@Test
	public void parseUnitConfigResponse() throws Exception {

		String xml = "";

		List<Integer> result = StringUtils.parseUnitConfigResponse(xml);
		assertTrue(result.size() == 0);

		xml = null;

		result = StringUtils.parseUnitConfigResponse(xml);
		assertTrue(result.size() == 0);

		xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<Message UtcTime=\"2019-10-03T11:02:37.808-04:00\" Version=\"1.0\">\r\n" + 
				"    <Response Class=\"0 - Success\" TransId=\"111.222.3.444-1\">\r\n" + 
				"        <getUnitConfig affiliateName=\"HBO -CAS\"\r\n" + 
				"            locationName=\"SUNRISE-CAS\" modelNumber=\"DSR7412\"\r\n" + 
				"            presetID=\"4\" unitAddress=\"000-11421-70022\" unitName=\"5555555555555555\">\r\n" + 
				"            <peerUAList>\r\n" + 
				"                <peerUA unitAddress=\"000-11421-70023\"/>\r\n" + 
				"            </peerUAList>\r\n" + 
				"            <tierList>\r\n" + 
				"            </tierList>\r\n" + 
				"            <serviceList>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"1\" vcNumber=\"2520\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"2\" vcNumber=\"2590\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"3\" vcNumber=\"2540\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"4\" vcNumber=\"2410\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"5\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"6\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"7\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"8\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"9\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"10\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"11\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"12\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"            </serviceList>\r\n" + 
				"        </getUnitConfig>\r\n" + 
				"    </Response>\r\n" + 
				"</Message>";

		result = StringUtils.parseUnitConfigResponse(xml);
		assertTrue(result.size() == 0);

		xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<Message UtcTime=\"2019-10-03T11:02:37.808-04:00\" Version=\"1.0\">\r\n" + 
				"    <Response Class=\"0 - Success\" TransId=\"111.222.3.444-1\">\r\n" + 
				"        <getUnitConfig affiliateName=\"HBO -CAS\"\r\n" + 
				"            locationName=\"SUNRISE-CAS\" modelNumber=\"DSR7412\"\r\n" + 
				"            presetID=\"4\" unitAddress=\"000-11421-70022\" unitName=\"5555555555555555\">\r\n" + 
				"            <peerUAList>\r\n" + 
				"                <peerUA unitAddress=\"000-11421-70023\"/>\r\n" + 
				"            </peerUAList>\r\n" + 
				"            <tierList>\r\n" + 
				"                <tier tier=\"250\"/>\r\n" + 
				"                <tier tier=\"259\"/>\r\n" + 
				"            </tierList>\r\n" + 
				"            <serviceList>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"1\" vcNumber=\"2520\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"2\" vcNumber=\"2590\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"3\" vcNumber=\"2540\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"4\" vcNumber=\"2410\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"5\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"6\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"7\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"8\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"9\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"10\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"11\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"12\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"            </serviceList>\r\n" + 
				"        </getUnitConfig>\r\n" + 
				"    </Response>\r\n" + 
				"</Message>";

		result = StringUtils.parseUnitConfigResponse(xml);
		assertTrue(result.size() > 0);
		assertTrue(result.size() == 2);
		assertTrue(result.get(0) == 250);
		assertTrue(result.get(1) == 259);

		xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<Message UtcTime=\"2019-10-03T11:02:37.808-04:00\" Version=\"1.0\">\r\n" + 
				"    <Response Class=\"0 - Success\" TransId=\"111.222.3.444-1\">\r\n" + 
				"        <getUnitConfig affiliateName=\"HBO -CAS\"\r\n" + 
				"            locationName=\"SUNRISE-CAS\" modelNumber=\"DSR7412\"\r\n" + 
				"            presetID=\"4\" unitAddress=\"000-11421-70022\" unitName=\"5555555555555555\">\r\n" + 
				"            <peerUAList>\r\n" + 
				"                <peerUA unitAddress=\"000-11421-70023\"/>\r\n" + 
				"            </peerUAList>\r\n" + 
				"            <tierList>\r\n" + 
				"                <tier tier=\"241\"/>\r\n" + 
				"                <tier tier=\"259\"/>\r\n" + 
				"                <tier tier=\"252\"/>\r\n" + 
				"                <tier tier=\"253\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"                <tier tier=\"267\"/>\r\n" + 
				"                <tier tier=\"267\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"            </tierList>\r\n" + 
				"            <serviceList>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"1\" vcNumber=\"2520\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"2\" vcNumber=\"2590\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"3\" vcNumber=\"2540\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"4\" vcNumber=\"2410\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"5\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"6\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"7\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"8\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"9\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"10\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"11\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"12\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"            </serviceList>\r\n" + 
				"        </getUnitConfig>\r\n" + 
				"    </Response>\r\n" + 
				"</Message>";

		result = StringUtils.parseUnitConfigResponse(xml);
		assertTrue(result.size() > 0);
		assertTrue(result.size() == 12);
		assertTrue(result.get(11) == 254);
		assertTrue(result.get(0) == 241);

		xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<Message UtcTime=\"2019-10-03T11:02:37.808-04:00\" Version=\"1.0\">\r\n" + 
				"    <Response Class=\"0 - Success\" TransId=\"111.222.3.444-1\">\r\n" + 
				"        <getUnitConfig affiliateName=\"HBO -CAS\"\r\n" + 
				"            locationName=\"SUNRISE-CAS\" modelNumber=\"DSR7412\"\r\n" + 
				"            presetID=\"4\" unitAddress=\"000-11421-70022\" unitName=\"5555555555555555\">\r\n" + 
				"            <peerUAList>\r\n" + 
				"                <peerUA unitAddress=\"000-11421-70023\"/>\r\n" + 
				"            </peerUAList>\r\n" + 
				"            <tierList>\r\n" + 
				"                <tier tier=\"241\"/>\r\n" + 
				"                <tier tier=\"259\"/>\r\n" + 
				"                <tier tier=\"252\"/>\r\n" + 
				"                <tier tier=\"253\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"                <tier tier=\"267\"/>\r\n" + 
				"                <tier tier=\"267\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"            </tierList>\r\n" + 
				"            <serviceList>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"1\" vcNumber=\"2520\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"2\" vcNumber=\"2590\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"3\" vcNumber=\"2540\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"4\" vcNumber=\"2410\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"5\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"6\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"7\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"8\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"9\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"10\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"11\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"12\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"            </serviceList>\r\n" + 
				"        </getUnitConfig>\r\n" + 
				"    </Response>\r\n" + 
				"</Message>";
		

		result = StringUtils.parseUnitConfigResponse(xml);
		assertTrue(result.size() > 0);
		assertTrue(result.size() == 12);
		assertTrue(result.get(9) == 267);
		assertTrue(result.get(10) == 267);
		
		xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<Message UtcTime=\"2019-10-03T11:02:37.808-04:00\" Version=\"1.0\">\r\n" + 
				"    <Response Class=\"0 - Success\" TransId=\"111.222.3.444-1\">\r\n" + 
				"        <getUnitConfig affiliateName=\"HBO -CAS\"\r\n" + 
				"            locationName=\"SUNRISE-CAS\" modelNumber=\"DSR7412\"\r\n" + 
				"            presetID=\"4\" unitAddress=\"000-11421-70022\" unitName=\"5555555555555555\">\r\n" + 
				"            <peerUAList>\r\n" + 
				"                <peerUA unitAddress=\"000-11421-70023\"/>\r\n" + 
				"            </peerUAList>\r\n" + 
				"            <tierList>\r\n" + 
				"                <tier tier=\"241\"/>\r\n" + 
				"                <tier tier=\"259\"/>\r\n" + 
				"                <tier tier=\"252\"/>\r\n" + 
				"                <tier tier=\"253\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"            </tierList>\r\n" + 
				"            <serviceList>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"1\" vcNumber=\"2520\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"2\" vcNumber=\"2590\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"3\" vcNumber=\"2540\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"4\" vcNumber=\"2410\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"5\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"6\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"7\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"8\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"9\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"10\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"11\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"12\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"            </serviceList>\r\n" + 
				"        </getUnitConfig>\r\n" + 
				"    </Response>\r\n" + 
				"</Message>";
		
		result = StringUtils.parseUnitConfigResponse(xml);
		assertTrue(result.size() == 5);
		
		xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<Message UtcTime=\"2019-10-03T11:02:37.808-04:00\" Version=\"1.0\">\r\n" + 
				"    <Response Class=\"0 - Success\" TransId=\"111.222.3.444-1\">\r\n" + 
				"        <getUnitConfig affiliateName=\"HBO -CAS\"\r\n" + 
				"            locationName=\"SUNRISE-CAS\" modelNumber=\"DSR7412\"\r\n" + 
				"            presetID=\"4\" unitAddress=\"000-11421-70022\" unitName=\"5555555555555555\">\r\n" + 
				"            <peerUAList>\r\n" + 
				"                <peerUA unitAddress=\"000-11421-70023\"/>\r\n" + 
				"            </peerUAList>\r\n" + 
				"            <tierList>\r\n" + 
				"                <tier tier=\"241\"/>\r\n" + 
				"                <tier tier=\"259\"/>\r\n" + 
				"                <tier tier=\"252\"/>\r\n" + 
				"                <tier tier=\"253\"/>\r\n" + 
				"                <tier tier=\"254\"/>\r\n" + 
				"            </tierList>\r\n" + 
				"            <serviceList>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"1\" vcNumber=\"2520\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"2\" vcNumber=\"2590\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"3\" vcNumber=\"2540\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"4\" vcNumber=\"2410\" vcTable=\"106\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"5\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"6\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"7\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"8\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"9\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"10\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"11\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"                <service hdEnable=\"false\" passthruEnable=\"false\"\r\n" + 
				"                    sdEnable=\"false\" transcoderID=\"12\" vcNumber=\"0\" vcTable=\"0\"/>\r\n" + 
				"            </serviceList>\r\n" + 
				"        </getUnitConfig>\r\n" + 
				"    </Response>\r\n" + 
				"</Message>";
		
		
		result = StringUtils.parseUnitConfigResponse(xml);
		assertTrue(result.size() > 0);
		assertTrue(result.size() == 5);
		assertTrue(result.get(0) == 241);
		assertTrue(result.get(1) == 259);
		assertTrue(result.get(2) == 252);
		assertTrue(result.get(3) == 253);
		assertTrue(result.get(4) == 254);

	}
	
	@Test
	public void compareTiers(){
		
		List<Integer> bncTiers = new ArrayList<Integer>();
		
		
		List<HBOTierDTO> listTiers = new ArrayList<HBOTierDTO>();
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(20, "test2", "create", 0, 0));
		listTiers.add(new HBOTierDTO(30, "test3", "create", 0, 0));
		
		List<HBOTierDTO> result = StringUtils.compareTiers(bncTiers, listTiers);
		
		assertTrue(result.size() == 3);
		assertTrue(result.get(0).getNumber() == 10);
		assertTrue(result.get(0).getOperation() == "create");
		assertTrue(result.get(1).getNumber() == 20);
		assertTrue(result.get(1).getOperation() == "create");
		assertTrue(result.get(2).getNumber() == 30);
		assertTrue(result.get(2).getOperation() == "create");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 0);
		
		
	    bncTiers.add(10);
		bncTiers.add(20);
		
		listTiers = new ArrayList<HBOTierDTO>();
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 2);
		assertTrue(result.get(0).getNumber() == 10);
		assertTrue(result.get(0).getOperation() == "delete");
		assertTrue(result.get(1).getNumber() == 20);
		assertTrue(result.get(1).getOperation() == "delete");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(20, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(30, "test1", "create", 0, 0));
		
		bncTiers.add(10);
		bncTiers.add(40);
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 4);
		assertTrue(result.get(0).getNumber() == 10);
		assertTrue(result.get(0).getOperation() == "create");
		assertTrue(result.get(1).getNumber() == 20);
		assertTrue(result.get(1).getOperation() == "create");
		assertTrue(result.get(2).getNumber() == 30);
		assertTrue(result.get(2).getOperation() == "create");
		assertTrue(result.get(3).getNumber() == 40);
		assertTrue(result.get(3).getOperation() == "delete");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(40, "test1", "create", 0, 0));
		
		bncTiers.add(10);
		bncTiers.add(20);
		bncTiers.add(30);
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 4);
		assertTrue(result.get(0).getNumber() == 10);
		assertTrue(result.get(0).getOperation() == "create");
		assertTrue(result.get(1).getNumber() == 40);
		assertTrue(result.get(1).getOperation() == "create");
		assertTrue(result.get(2).getNumber() == 20);
		assertTrue(result.get(2).getOperation() == "delete");
		assertTrue(result.get(3).getNumber() == 30);
		assertTrue(result.get(3).getOperation() == "delete");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(40, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(70, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(80, "test1", "create", 0, 0));
		
		bncTiers.add(10);
		bncTiers.add(20);
		bncTiers.add(30);
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 6);
		assertTrue(result.get(0).getNumber() == 10);
		assertTrue(result.get(0).getOperation() == "create");
		assertTrue(result.get(1).getNumber() == 40);
		assertTrue(result.get(1).getOperation() == "create");
		assertTrue(result.get(2).getNumber() == 70);
		assertTrue(result.get(2).getOperation() == "create");
		assertTrue(result.get(3).getNumber() == 80);
		assertTrue(result.get(3).getOperation() == "create");
		assertTrue(result.get(4).getNumber() == 20);
		assertTrue(result.get(4).getOperation() == "delete");
		assertTrue(result.get(5).getNumber() == 30);
		assertTrue(result.get(5).getOperation() == "delete");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(40, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(70, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(80, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(90, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(100, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(110, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(120, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(130, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(140, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(150, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(160, "test1", "create", 0, 0));
		
		bncTiers.add(10);
		bncTiers.add(20);
		bncTiers.add(30);
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 14);
		assertTrue(result.get(0).getNumber() == 10);
		assertTrue(result.get(0).getOperation() == "create");
		assertTrue(result.get(1).getNumber() == 40);
		assertTrue(result.get(1).getOperation() == "create");
		assertTrue(result.get(2).getNumber() == 70);
		assertTrue(result.get(2).getOperation() == "create");
		assertTrue(result.get(3).getNumber() == 80);
		assertTrue(result.get(3).getOperation() == "create");
		assertTrue(result.get(4).getNumber() == 90);
		assertTrue(result.get(4).getOperation() == "create");
		assertTrue(result.get(5).getNumber() == 100);
		assertTrue(result.get(5).getOperation() == "create");
		assertTrue(result.get(6).getNumber() == 110);
		assertTrue(result.get(6).getOperation() == "create");
		assertTrue(result.get(7).getNumber() == 120);
		assertTrue(result.get(7).getOperation() == "create");
		assertTrue(result.get(8).getNumber() == 130);
		assertTrue(result.get(8).getOperation() == "create");
		assertTrue(result.get(9).getNumber() == 140);
		assertTrue(result.get(9).getOperation() == "create");
		assertTrue(result.get(10).getNumber() == 150);
		assertTrue(result.get(10).getOperation() == "create");
		assertTrue(result.get(11).getNumber() == 160);
		assertTrue(result.get(11).getOperation() == "create");
		assertTrue(result.get(12).getNumber() == 20);
		assertTrue(result.get(12).getOperation() == "delete");
		assertTrue(result.get(13).getNumber() == 30);
		assertTrue(result.get(13).getOperation() == "delete");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(20, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(30, "test1", "create", 0, 0));
		
		
		bncTiers.add(10);
		bncTiers.add(20);
		bncTiers.add(30);
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 3);
		assertTrue(result.get(0).getNumber() == 10);
		assertTrue(result.get(0).getOperation() == "create");
		assertTrue(result.get(1).getNumber() == 20);
		assertTrue(result.get(1).getOperation() == "create");
		assertTrue(result.get(2).getNumber() == 30);
		assertTrue(result.get(2).getOperation() == "create");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(20, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(30, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(40, "test1", "create", 0, 0));
		
		
		bncTiers.add(10);
		bncTiers.add(20);
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 4);
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(20, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(30, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(40, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(50, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(60, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(70, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(80, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(90, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(100, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(110, "test1", "create", 0, 0));
		listTiers.add(new HBOTierDTO(120, "test1", "create", 0, 0));
		
		
		bncTiers.add(10);
		bncTiers.add(20);
		bncTiers.add(130);
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 13);
		assertTrue(result.get(2).getNumber() == 30);
		assertTrue(result.get(2).getOperation() == "create");
		assertTrue(result.get(11).getNumber() == 120);
		assertTrue(result.get(11).getOperation() == "create");
		assertTrue(result.get(12).getNumber() == 130);
		assertTrue(result.get(12).getOperation() == "delete");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		//Concentrating 4470 case as it has just 1 transcoder 
		
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		bncTiers.add(10);
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 1);
		assertTrue(result.get(0).getNumber() == 10);
		assertTrue(result.get(0).getOperation() == "create");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		bncTiers.add(10);
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 1);
		assertTrue(result.get(0).getNumber() == 10);
		assertTrue(result.get(0).getOperation() == "create");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
		listTiers.add(new HBOTierDTO(10, "test1", "create", 0, 0));
		bncTiers.add(20);
		
		result = StringUtils.compareTiers(bncTiers, listTiers);
		assertTrue(result.size() == 2);
		assertTrue(result.get(0).getNumber() == 10);
		assertTrue(result.get(0).getOperation() == "create");
		assertTrue(result.get(1).getNumber() == 20);
		assertTrue(result.get(1).getOperation() == "delete");
		
		bncTiers.clear();
		listTiers.clear();
		result.clear();
		
	}

	// too lazy to create the dto from scratch so i used this and gson to create
	// it for me :)
	private static final String JSON_REQUEST_EXAMPLE = "{\r\n" + "	\"affiliateName\": \"HBOLA\",\r\n"
			+ "	\"locationName\": \"HBOLA\",\r\n" + "	\"modelNumber\": \"DSR7412\",\r\n"
			+ "	\"presetId\": \"5\",\r\n" + "	\"decimalUnitAddress\": \"0001142161408\",\r\n"
			+ "	\"secondaryDecimalUnitAdress\": \"0001142161409\",\r\n" + "	\"name\": \"test 0001142161408\",\r\n"
			+ "	\"tiers\": [{\r\n" + "		\"number\": \"241\"\r\n" + "	}, {\r\n" + "		\"number\": \"242\"\r\n"
			+ "	}, {\r\n" + "		\"number\": \"243\"\r\n" + "	}],\r\n" + "	\"services\": [{\r\n"
			+ "		\"hdEnable\": \"false\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"1\",\r\n"
			+ "		\"vcNumber\": \"2410\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"false\",\r\n" + "		\"transcoderID\": \"2\",\r\n"
			+ "		\"vcNumber\": \"2420\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"false\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"3\",\r\n"
			+ "		\"vcNumber\": \"2430\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"4\",\r\n"
			+ "		\"vcNumber\": \"2440\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"5\",\r\n"
			+ "		\"vcNumber\": \"2450\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"6\",\r\n"
			+ "		\"vcNumber\": \"2460\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"7\",\r\n"
			+ "		\"vcNumber\": \"2470\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"8\",\r\n"
			+ "		\"vcNumber\": \"2480\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"9\",\r\n"
			+ "		\"vcNumber\": \"2490\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"10\",\r\n"
			+ "		\"vcNumber\": \"0\",\r\n" + "		\"vcTable\": \"0\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"11\",\r\n"
			+ "		\"vcNumber\": \"0\",\r\n" + "		\"vcTable\": \"0\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"12\",\r\n"
			+ "		\"vcNumber\": \"0\",\r\n" + "		\"vcTable\": \"0\"\r\n" + "	}]\r\n" + "\r\n" + "}";

	// this one has the first service to delete then the second service is
	// authorized
	private static final String JSON_REQUEST_EXAMPLE2 = "{\r\n" + "	\"affiliateName\": \"HBOLA\",\r\n"
			+ "	\"locationName\": \"HBOLA\",\r\n" + "	\"modelNumber\": \"DSR7412\",\r\n"
			+ "	\"presetId\": \"5\",\r\n" + "	\"decimalUnitAddress\": \"0001142161408\",\r\n"
			+ "	\"secondaryDecimalUnitAdress\": \"0001142161409\",\r\n" + "	\"name\": \"test 0001142161408\",\r\n"
			+ "	\"tiers\": [{\r\n" + "		\"number\": \"241\"\r\n" + "	}, {\r\n" + "		\"number\": \"242\"\r\n"
			+ "	}, {\r\n" + "		\"number\": \"243\"\r\n" + "	}],\r\n" + "	\"services\": [{\r\n"
			+ "		\"hdEnable\": \"false\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"1\",\r\n"
			+ "		\"vcNumber\": \"2410\",\r\n" + "		\"vcTable\": \"106\"\r\n" +
			// " \"operation\": \"delete\"\r\n" +
			"	}, {\r\n" + "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"false\",\r\n" + "		\"transcoderID\": \"2\",\r\n"
			+ "		\"vcNumber\": \"2420\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"false\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"3\",\r\n"
			+ "		\"vcNumber\": \"2430\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"4\",\r\n"
			+ "		\"vcNumber\": \"2440\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"5\",\r\n"
			+ "		\"vcNumber\": \"2450\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"6\",\r\n"
			+ "		\"vcNumber\": \"2460\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"7\",\r\n"
			+ "		\"vcNumber\": \"2470\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"8\",\r\n"
			+ "		\"vcNumber\": \"2480\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"9\",\r\n"
			+ "		\"vcNumber\": \"2490\",\r\n" + "		\"vcTable\": \"106\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"10\",\r\n"
			+ "		\"vcNumber\": \"0\",\r\n" + "		\"vcTable\": \"0\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"11\",\r\n"
			+ "		\"vcNumber\": \"0\",\r\n" + "		\"vcTable\": \"0\"\r\n" + "	}, {\r\n"
			+ "		\"hdEnable\": \"true\",\r\n" + "		\"passthruEnable\": \"true\",\r\n"
			+ "		\"sdEnable\": \"true\",\r\n" + "		\"transcoderID\": \"12\",\r\n"
			+ "		\"vcNumber\": \"0\",\r\n" + "		\"vcTable\": \"0\"\r\n" + "	}]\r\n" + "\r\n";
}
