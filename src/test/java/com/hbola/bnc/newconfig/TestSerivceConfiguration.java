package com.hbola.bnc.newconfig;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages={"com.hbola.bnc"})
public class TestSerivceConfiguration {
}
