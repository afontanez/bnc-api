package com.hbola.bnc.utils;

public class LoggerFormatter {

	public static String logError(String message, String payload) {
		
		message = message == null ? "UNKNOWN" : message;
		payload = payload == null ? "UNKNOWN" : payload;
		
		return "ERROR: " + message + ". " + payload + ". ";
	}
	
	public static String logInfoStarted(String message, String payload) {
		
		message = message == null ? "UNKNOWN" : message;
		payload = payload == null ? "UNKNOWN" : payload;

		return "START: " + message + ". " + payload + ". ";
	}
				
	public static String logInfoEnded(String message, String response) {
		message = message == null ? "UNKNOWN" : message;
		response = response == null ? "UNKNOWN" : response;		

		return "END: " + message + ". " + response + ". ";
	}	
		
}
