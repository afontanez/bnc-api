package com.hbola.bnc.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringEscapeUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.hbola.bnc.config.ConfigCache;
import com.hbola.bnc.dto.HBOIrdDTO;
import com.hbola.bnc.dto.HBOServiceDTO;
import com.hbola.bnc.dto.HBOTierDTO;

public class StringUtils {

	private static final String RESPONSE_INFO = "responseInfo=";
	private static final String FAILURE = "Failure";
	private static final String END = "/>";

	private static String getFormattedDate() {

		String formattedDate = "";
		try {
			Date currentDate = new Date();
			SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DD'T'hh:mm:ss.S'Z'");
			formattedDate = format.format(currentDate);
		} catch (Exception e) {
			formattedDate = "";
		}
		return formattedDate;
	}

	public static String setupDSR74xxDSR4470XMLRequest(HBOIrdDTO dto, Integer transactionId) {

		String xml = "";
		if (dto != null) {

			if (transactionId == null || transactionId < 0)
				transactionId = 0;

			xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<Message UtcTime=\"" + getFormattedDate()
					+ "\" Version=\"1.0\">" + "    <Request TransId=\"111.222.3.444-" + transactionId + "\">"
					+ "        <setUnitConfig affiliateName=\""
					+ ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_NAME") + "\" locationName=\""
					+ ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_LOCATION_NAME") + "\" modelNumber=\""
					+ dto.getModelNumber() + "\" presetID=\"" + dto.getPresetId() + "\" unitAddress=\""
					+ dto.getDecimalUnitAddress() + "\" unitName=\"" + dto.getName() + "\">";

			if (!dto.getModelNumber().equals("DSR4470")) {
				xml += "        <peerUAList>" + "                <peerUA unitAddress=\""
						+ dto.getSecondaryDecimalUnitAdress() + "\"/>" + "            </peerUAList>";
			}

			xml += "        <tierList>";
			for (HBOTierDTO tier : dto.getTiers()) {

				if (tier.getOperation() != null && tier.getOperation().equalsIgnoreCase("delete")) {
					xml += "<tier tier=\"" + (tier.getNumber() == null ? "" : tier.getNumber())
							+ "\" markForDelete=\"true\" />";
				} else {
					xml += "<tier tier=\"" + (tier.getNumber() == null ? "" : tier.getNumber()) + "\"/>";
				}
			}

			xml += "</tierList>" + "            <serviceList>";

			for (HBOServiceDTO service : dto.getServices()) {

				if (service.getOperation() != null && service.getOperation().equalsIgnoreCase("delete")) {
					xml += "<service hdEnable=\"" + (service.getHdEnable() == null ? false : service.getHdEnable())
							+ "\" passthruEnable=\""
							+ (service.getPassthruEnable() == null ? false : service.getPassthruEnable())
							+ "\" sdEnable=\"" + (service.getSdEnable() == null ? false : service.getSdEnable())
							+ "\" transcoderID=\"" + (service.getTranscoderID() == null ? 0 : service.getTranscoderID())
							+ "\" vcNumber=\"" + (service.getVcNumber() == null ? 0 : service.getVcNumber())
							+ "\" vcTable=\"" + (service.getVcTable() == null ? 0 : service.getVcTable())
							+ "\" markForDelete=\"true\" />";
				} else {
					xml += "<service hdEnable=\"" + (service.getHdEnable() == null ? false : service.getHdEnable())
							+ "\" passthruEnable=\""
							+ (service.getPassthruEnable() == null ? false : service.getPassthruEnable())
							+ "\" sdEnable=\"" + (service.getSdEnable() == null ? false : service.getSdEnable())
							+ "\" transcoderID=\"" + (service.getTranscoderID() == null ? 0 : service.getTranscoderID())
							+ "\" vcNumber=\"" + (service.getVcNumber() == null ? 0 : service.getVcNumber())
							+ "\" vcTable=\"" + (service.getVcTable() == null ? 0 : service.getVcTable()) + "\"/>";
				}
			}

			// mark the rest of the services for this decoder as delete
			for (int i = dto.getServices().size() + 1; i <= dto.getDefaultNumberOfServices(); i++) {
				xml += "<service transcoderID=\"" + i + "\" vcNumber=\"0\" vcTable=\"0\" markForDelete=\"true\" />";
			}

			xml += "     </serviceList>" + "        </setUnitConfig>" + "    </Request>" + "</Message>";
		}

		return xml;
	}

	public static String getUnitConfiguration74xx4470(HBOIrdDTO dto, Integer transactionId) {
		// TODO Auto-generated method stub
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<Message UtcTime=\"" + getFormattedDate()
				+ "\" Version=\"1.0\">" + "    <Request TransId=\"111.222.3.444-" + transactionId + "\">"
				+ "        <getUnitConfig unitAddress=\"" + dto.getDecimalUnitAddress() + "\"/>" + "</Request>"
				+ "</Message>";

		return xml;
	}

	public static String validateAndFormat(String label, String value) throws Exception {

		if (label == null || label.isEmpty())
			label = "<missing label>";

		String returnValue = "";
		if (value != null && !value.isEmpty()) {
			returnValue = value.replaceAll("-", "");
			returnValue = StringUtils.truncateTo13(returnValue);
		} else {
			throw new Exception(label + " cannot be null or empty");
		}

		return returnValue;
	}

	public static String truncateTo13(String value) {

		String returnValue = "";
		try {
			if (value != null && !value.isEmpty() && value.length() > 13) {
				returnValue = value.substring(0, 13);
			} else {
				returnValue = value;
			}
		} catch (Exception e) {
			returnValue = "";
		}

		return returnValue;
	}

	// <?xml version="1.0" encoding="UTF-8"?><Message
	// UtcTime="2017-12-08T18:48:30.526Z" Version="1.0"><Response Class="1 -
	// Failure" TransId="111.222.3.444-3"><setUnitConfig responseInfo="Invalid
	// Affilite and/or Location:HBOLA:HBOLA"/></Response></Message>
	// <?xml version="1.0" encoding="UTF-8"?><Message
	// UtcTime="2017-12-18T22:42:04.215Z" Version="1.0"><Response Class="2 -
	// Failure (XML parsing):java.lang.NumberFormatException: For input string:
	// &quot;null&quot;"/></Message>
	// <?xml version="1.0" encoding="UTF-8"?><Message
	// UtcTime="2018-02-16T19:32:09.063Z" Version="1.0"><Response Class="2 -
	// failure" TransId="111.222.3.444-6"><setUnitConfig responseInfo="Exception
	// Type: BNCEjbStatusException&#xa;Why: {[26-6-27] The unit address
	// (000-11111-11111) is not in a valid address range.}&#xa;Where:
	// BNCEjbStatusException&#xa;"/></Response></Message>
	public static String extractMessageFromBNCResponse(String message) {

		String returnValue = "";
		try {
			if (message != null && !message.isEmpty()) {

				message = StringEscapeUtils.unescapeXml(message);

				if (message.contains(RESPONSE_INFO)) {
					returnValue = parseMessage(message, RESPONSE_INFO);
				} else if (message.contains(FAILURE)) {
					returnValue = parseMessage(message, FAILURE);
				}
			}
		} catch (Exception e) {
			returnValue = "";
		}
		return returnValue;
	}

	private static String parseMessage(String message, String messageKey) {

		String returnValue = "";
		int startIndex = message.indexOf(messageKey);
		if (startIndex > 0) {
			int endIndex = message.indexOf(END, startIndex);
			if (endIndex > 0) {
				returnValue = message.substring(startIndex, endIndex);
				returnValue = returnValue.replace(messageKey, "");
			}
		}
		return returnValue;
	}

	public static void validateServices(List<HBOServiceDTO> list) throws Exception {

		if (list != null && !list.isEmpty()) {

			int transcoder = 1;
			for (HBOServiceDTO dto : list) {
				if (dto.getVcNumber() == null /*
												 * ||
												 * !dto.getVcNumber().replaceAll
												 * ("^\\d{4}$", "").isEmpty()
												 */)
					throw new Exception(
							"Transcoder " + transcoder + " : vch number must not be null and must be numeric");
				if (dto.getVcTable() == null /*
												 * ||
												 * !dto.getVcTable().replaceAll(
												 * "^\\d{3}$", "").isEmpty()
												 */)
					throw new Exception(
							"Transcoder " + transcoder + " : vc table must not be null and must be numeric");
				transcoder++;
			}
		} else {
			throw new Exception("No Services Setup For This Decoder");
		}

	}

	public static String setupDSR74xxDSR4470XMLTripRequest(String unitAddress, Integer transactionId) throws Exception {

		String returnValue = "";
		if (unitAddress != null && !unitAddress.isEmpty()) {
			if (transactionId == null || transactionId < 0)
				transactionId = 0;
			returnValue = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Message UtcTime=\"" + getFormattedDate()
					+ "\" Version=\"1.0\"><Request TransId=\"111.222.3.444-" + transactionId
					+ "\"><setUnitConfig unitAddress=\"" + unitAddress + "\"/></Request></Message>";
		} else {
			throw new Exception("Could not create trip message. Unit address is null or empty");
		}

		return returnValue;
	}

	public static List<Integer> parseUnitConfigResponse(String unitConfigResponse) throws Exception {
		// TODO Auto-generated method stub

		if (unitConfigResponse == "" || unitConfigResponse == null) {
			return new ArrayList<Integer>();
		}

		String feature = "";

		List<Integer> bncResult = new ArrayList<Integer>();

		try {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setNamespaceAware(true);
			feature = "http://apache.org/xml/features/disallow-doctype-decl";
			dbFactory.setFeature(feature, true);
			dbFactory.setXIncludeAware(false);
			dbFactory.setExpandEntityReferences(false);

			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			byte[] utf8Bytes = unitConfigResponse.getBytes("UTF-8");
			ByteArrayInputStream bais = new ByteArrayInputStream(utf8Bytes);
			Document doc = dBuilder.parse(bais);

			NodeList nList = doc.getElementsByTagName("tierList");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
				Element eElement = (Element) nNode;

				for (int i = 0; i < eElement.getChildNodes().getLength(); i++) {

					NamedNodeMap test = eElement.getChildNodes().item(i).getAttributes();
					if (test != null) {
						Node val = test.item(0);
						Integer result = Integer.valueOf(val.getNodeValue());
						bncResult.add(result);
					}
				}

			}

		} catch (Exception e) {
			throw new Exception(e);
		}

		return bncResult;

	}

	public static List<HBOTierDTO> compareTiers(List<Integer> bncTiers, List<HBOTierDTO> newTiers) {

		List<HBOTierDTO> finalTiers = new ArrayList<HBOTierDTO>();
		List<HBOTierDTO> goodDasTiers = new ArrayList<HBOTierDTO>();
		List<Integer> goodBncTiers = new ArrayList<Integer>();

		if (bncTiers.size() == 0) {
			finalTiers.addAll(newTiers);
			return finalTiers;
		}

		if (newTiers.size() == 0) {
			
			for (Integer bncTier : bncTiers){
				HBOTierDTO dto = new HBOTierDTO(bncTier, "defaultTier", "delete", 0, 0);
				finalTiers.add(dto);
			}

			return finalTiers;
		}

		
		for (HBOTierDTO newTier : newTiers){
			for (Integer bncTier : bncTiers){
				
				if (newTier.getNumber().intValue() == bncTier.intValue()){
					finalTiers.add(newTier);
					goodDasTiers.add(newTier);
					goodBncTiers.add(bncTier);
				}
			}
		}
		
		newTiers.removeAll(goodDasTiers);
		finalTiers.addAll(newTiers);
		
		bncTiers.removeAll(goodBncTiers);
		
		for (Integer bncTier : bncTiers){
			finalTiers.add(new HBOTierDTO(bncTier, "defaultTier", "delete", 0, 0));
		}
		
		return finalTiers;
	}

	public static String prepareDeleteIrd(HBOIrdDTO dto) {
		// TODO Auto-generated method stub
		 
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<Message UtcTime=\"2015-06-24T14:24:09.759-07:00\" version=1.0\">\r\n" + 
				"<Request TransId=\"168.84.56.24-123459\">\r\n" + 
				"<setUnitConfig unitAddress=\"" + dto.getDecimalUnitAddress() + "\" markForDelete=\"true\"/>\r\n" + 
				"</Request>\r\n" + 
				"</Message>";
		return xml;
	}
}
