package com.hbola.bnc.dto;

public class HBOServiceDTO {

	private Integer transcoderID; 
	private Boolean hdEnable;
	private Boolean sdEnable;
	private Boolean passthruEnable;
	private String vcNumber;
	private String vcTable;
	private String operation;
	private Integer sdConvertMethod;
	private Integer sdConvertAspectRatio;
	private String unitAddress;//in case of 4410MD
	
	public String getUnitAddress() {
		return unitAddress;
	}
	public void setUnitAddress(String unitAddress) {
		this.unitAddress = unitAddress;
	}
	public Integer getSdConvertAspectRatio() {
		return sdConvertAspectRatio;
	}
	public void setSdConvertAspectRatio(Integer sdConvertAspectRatio) {
		this.sdConvertAspectRatio = sdConvertAspectRatio;
	}
	public Integer getSdConvertMethod() {
		return sdConvertMethod;
	}
	public void setSdConvertMethod(Integer sdConvertMethod) {
		this.sdConvertMethod = sdConvertMethod;
	}

	public Integer getTranscoderID() {
		return transcoderID;
	}
	public void setTranscoderID(Integer transcoderID) {
		this.transcoderID = transcoderID;
	}
	public Boolean getHdEnable() {
		return hdEnable;
	}
	public void setHdEnable(Boolean hdEnable) {
		this.hdEnable = hdEnable;
	}
	public Boolean getSdEnable() {
		return sdEnable;
	}
	public void setSdEnable(Boolean sdEnable) {
		this.sdEnable = sdEnable;
	}
	public Boolean getPassthruEnable() {
		return passthruEnable;
	}
	public void setPassthruEnable(Boolean passthruEnable) {
		this.passthruEnable = passthruEnable;
	}
	public String getVcNumber() {
		return vcNumber;
	}
	public void setVcNumber(String vcNumber) {
		this.vcNumber = vcNumber;
	}
	public String getVcTable() {
		return vcTable;
	}
	public void setVcTable(String vcTable) {
		this.vcTable = vcTable;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	
	
}
