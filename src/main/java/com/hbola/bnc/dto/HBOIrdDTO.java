package com.hbola.bnc.dto;

import java.util.List;

public class HBOIrdDTO {
				
	private String affiliateName;
	private String locationName;
	private String modelNumber;
	private String presetId;
	private String decimalUnitAddress;
	private String secondaryDecimalUnitAdress;
	private String iRTUnitAddress;
	private String iRTSerialNumber;
	private String name;
	private int setDefaultNumberOfServices;	
	private List<HBOTierDTO> tiers;
	private List<HBOServiceDTO> services;
	private Integer transportStreamOutput;
	private Boolean frontPanelEnabled;
	private Boolean controlPortEnabled;
	private Integer sdiOutput;

	public Integer getSdiOutput() {
		return sdiOutput;
	}
	public void setSdiOutput(Integer sdiOutput) {
		this.sdiOutput = sdiOutput;
	}
	public Boolean getControlPortEnabled() {
		return controlPortEnabled;
	}
	public void setControlPortEnabled(Boolean controlPortEnabled) {
		this.controlPortEnabled = controlPortEnabled;
	}
	public Boolean getFrontPanelEnabled() {
		return frontPanelEnabled;
	}
	public void setFrontPanelEnabled(Boolean frontPanelEnabled) {
		this.frontPanelEnabled = frontPanelEnabled;
	}
	public Integer getTransportStreamOutput() {
		return transportStreamOutput;
	}
	public void setTransportStreamOutput(Integer transportStreamOutput) {
		this.transportStreamOutput = transportStreamOutput;
	}
	public String getAffiliateName() {
		return affiliateName;
	}
	public void setAffiliateName(String affiliateName) {
		this.affiliateName = affiliateName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getModelNumber() {
		return modelNumber;
	}
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	public String getPresetId() {
		return presetId;
	}
	public void setPresetId(String presetId) {
		this.presetId = presetId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<HBOTierDTO> getTiers() {
		return tiers;
	}
	public void setTiers(List<HBOTierDTO> tiers) {
		this.tiers = tiers;
	}
	public List<HBOServiceDTO> getServices() {
		return services;
	}
	public void setServices(List<HBOServiceDTO> services) {
		this.services = services;
	}		
	public int getDefaultNumberOfServices() {
		return setDefaultNumberOfServices;
	}	
	public void setDefaultNumberOfServices(int setDefaultNumberOfServices) {
		this.setDefaultNumberOfServices = setDefaultNumberOfServices;
	}
	public String getDecimalUnitAddress() {
		return decimalUnitAddress;
	}
	public void setDecimalUnitAddress(String decimalUnitAddress) {
		this.decimalUnitAddress = decimalUnitAddress;
	}
	public String getSecondaryDecimalUnitAdress() {
		return secondaryDecimalUnitAdress;
	}
	public void setSecondaryDecimalUnitAdress(String secondaryDecimalUnitAdress) {
		this.secondaryDecimalUnitAdress = secondaryDecimalUnitAdress;
	}
	public String getIRTUnitAddress() {
		return iRTUnitAddress;
	}
	public void setIRTUnitAddress(String iRTUnitAddress) {
		this.iRTUnitAddress = iRTUnitAddress;
	}
	public String getIRTSerialNumber() {
		return iRTSerialNumber;
	}
	public void setIRTSerialNumber(String iRTSerialNumber) {
		this.iRTSerialNumber = iRTSerialNumber;
	}
	
	
}
