package com.hbola.bnc.dto;

public class HBOTierDTO {

	private Integer number;
	private String name;
	private String operation;
	private Integer isTemplateTierExtended;
	private Integer templateId;
	
	public HBOTierDTO(Integer number, String name, String operation, Integer isTemplateTierExtended,
			Integer templateId) {
		super();
		this.number = number;
		this.name = name;
		this.operation = operation;
		this.isTemplateTierExtended = isTemplateTierExtended;
		this.templateId = templateId;
	}
		
	public Integer getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	public Integer getIsTemplateTierExtended() {
		return isTemplateTierExtended;
	}
	public void setIsTemplateTierExtended(Integer isTemplateTierExtended) {
		this.isTemplateTierExtended = isTemplateTierExtended;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	
}
