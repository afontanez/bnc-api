package com.hbola.bnc.dto;

public class HBOTranscodersPreloadedDTO {

	private Integer transcoderId;
    private Boolean hdEnabled;
    private Boolean sdEnabled;
    private Boolean passthruEnabled;
    private String vcNumber;
    private String vcTable;
    private String operation;
    private Boolean authorized = false;
//    private HBOTranscodersPreloadedServiceDTO service = new HBOTranscodersPreloadedServiceDTO();    
    
    public HBOTranscodersPreloadedDTO(Integer transcoderID, Boolean hdEnabled, Boolean sdEnabled, Boolean passthruEnabled, String vcNumber, String vcTable, String operation) {

		this.transcoderId = transcoderID;  
		this.hdEnabled = hdEnabled; 
		this.sdEnabled = sdEnabled; 
		this.passthruEnabled = passthruEnabled; 
		this.vcNumber = vcNumber; 
		this.vcTable = vcTable; 
		this.operation = operation; 
	}
    
	public int getTranscoderId() {
		return transcoderId;
	}
	public void setTranscoderId(int transcoderId) {
		this.transcoderId = transcoderId;
	}
	public boolean isHdEnabled() {
		return hdEnabled;
	}
	public void setHdEnabled(boolean hdEnabled) {
		this.hdEnabled = hdEnabled;
	}
	public boolean isSdEnabled() {
		return sdEnabled;
	}
	public void setSdEnabled(boolean sdEnabled) {
		this.sdEnabled = sdEnabled;
	}
	public boolean isPassthruEnabled() {
		return passthruEnabled;
	}
	public void setPassthruEnabled(boolean passthruEnabled) {
		this.passthruEnabled = passthruEnabled;
	}
	public String getVcNumber() {
		return vcNumber;
	}
	public void setVcNumber(String vcNumber) {
		this.vcNumber = vcNumber;
	}
	public String getVcTable() {
		return vcTable;
	}
	public void setVcTable(String vcTable) {
		this.vcTable = vcTable;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public boolean isAuthorized() {
		return authorized;
	}
	public void setAuthorized(boolean authorized) {
		this.authorized = authorized;
	}
//	public HBOTranscodersPreloadedServiceDTO getService() {
//		return service;
//	}
//	public void setService(HBOTranscodersPreloadedServiceDTO services) {
//		this.service = services;
//	}
//	public void setServices(int[] services) {
//		
//		for (int service : services ) {
//			
//			HBOTranscodersPreloadedServiceDTO dto = new HBOTranscodersPreloadedServiceDTO(service);
//			this.services.add(dto);
//		}
//		
//	}
    
    
    
   
	
}
