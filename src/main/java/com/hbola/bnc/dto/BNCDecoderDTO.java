package com.hbola.bnc.dto;

import java.util.Date;

//used in the descrepancy report
public class BNCDecoderDTO {

	private String unitAddress;
	private Integer tierNumber;
	private String cableOperator;
	private String modelID;
	private String status;
	private String headend;
	private Long createdOn;
	
	public BNCDecoderDTO() {}
	
	public BNCDecoderDTO(String unitAddress, Integer tierNumber, String cableOperator, String modelID, String status, String headend, Long createdOn) {
		
		this.unitAddress = unitAddress;
		this.tierNumber = tierNumber;
		this.cableOperator = cableOperator;
		this.modelID = modelID;
		this.status = status;
		this.headend = headend;
		this.createdOn = createdOn;
	}

	public String print() {
		return this.modelID;
	}
	
	public String getUnitAddress() {
		return unitAddress;
	}

	public void setUnitAddress(String unitAddress) {
		this.unitAddress = unitAddress;
	}

	public Integer getTierNumber() {
		return tierNumber;
	}

	public void setTierNumber(Integer tierNumber) {
		this.tierNumber = tierNumber;
	}

	public String getCableOperator() {
		return cableOperator;
	}

	public void setCableOperator(String cableOperator) {
		this.cableOperator = cableOperator;
	}

	public String getModelID() {
		return modelID;
	}

	public void setModelID(String modelID) {
		this.modelID = modelID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHeadend() {
		return headend;
	}

	public void setHeadend(String headend) {
		this.headend = headend;
	}

	public Long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Long createdOn) {
		this.createdOn = createdOn;
	}

	
}
