package com.hbola.bnc.dto;

import java.util.List;

public class HBOPresetDTO {

	private String name;
	private String modelName;
	private Integer number;
	private List<HBOTranscodersPreloadedDTO> transcodersPreloaded;
	
	
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public List<HBOTranscodersPreloadedDTO> getTranscodersPreloaded() {
		return transcodersPreloaded;
	}
	public void setTranscodersPreloaded(List<HBOTranscodersPreloadedDTO> transcodersPreloaded) {
		this.transcodersPreloaded = transcodersPreloaded;
	}
	
	
	
	
	
	
	
}
