package com.hbola.bnc.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbola.bnc.dto.HBOIrdDTO;
import com.hbola.bnc.dto.HBOTierDTO;
import com.hbola.bnc.utils.StringUtils;
import com.motorola.bnc.common.shared.structures.ACP;
import com.motorola.bnc.common.shared.structures.Affiliate;
import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderModelType;
import com.motorola.bnc.common.shared.structures.Location;
import com.motorola.bnc.common.shared.structures.MAD;
import com.motorola.bnc.common.shared.structures.MultiAcpIRD;
import com.motorola.bnc.common.shared.structures.RatingRegion;
import com.motorola.bnc.proxy.external.BNCExternalAPIFactory;
import com.motorola.bnc.proxy.external.IExternalClientAPI;
import com.motorola.refarch.infrastruct.gendata.BaseOperatorGroup;
import com.motorola.refarch.infrastruct.gendata.BasePartition;
import com.motorola.refarch.infrastruct.gendata.BaseRegion;
import com.motorola.refarch.infrastruct.gendata.BaseTier;
import com.motorola.refarch.shared.auth.DCIIUnitFilter;

@Service("4410MD")
public class Legacy4410MDServiceImpl implements Legacy4410MDServiceI {

	@Autowired
	DefaultServicesI defaultServices;
	
	@Override
	public void tripIRD(String irdUnitAddress) throws Exception {
		
		IExternalClientAPI iExternalClientAPI = defaultServices.getBncConnection();
		DCIIUnitFilter unitFilter = new DCIIUnitFilter(irdUnitAddress);
			    		
		iExternalClientAPI.tripACP(unitFilter);
	}
	
	/* (non-Javadoc)
	 * @see com.hbola.bnc.services.Legacy4410MDServiceI#storeMD(com.hbola.bnc.dto.HBOIrdDTO)
	 */
	@Override
	public void storeMD(List<HBOIrdDTO> dtoList) throws Exception {
		
		IExternalClientAPI iExternalClientAPI = defaultServices.getBncConnection();
	
		try {			
			for (HBOIrdDTO dto : dtoList) {
		
				MAD newMAD = null;
				try {
					DCIIUnitFilter dCIIUnitFilter = new DCIIUnitFilter(dto.getIRTUnitAddress());
					newMAD = iExternalClientAPI.queryMAD(dCIIUnitFilter);//throws an exception if not found instead of returning true false;
					newMAD.setSerialNumber(dto.getIRTSerialNumber());
					newMAD.setName(dto.getIRTSerialNumber());
				}
				catch (Exception e) {
					//if exception the newNad stays null and we key off of that below
				}
			
				for (int i = 0; i < dto.getServices().size(); i++) {
					
					String unitAddress = StringUtils.validateAndFormat("DecimalUnitAddress", dto.getDecimalUnitAddress());
					
					ACP newACP = ACP.getBlank();
					boolean forceRegen = false;
					
					DCIIUnitFilter dCIIUnitFilter = new DCIIUnitFilter(unitAddress);
						
					if (iExternalClientAPI.acpExists(dCIIUnitFilter)) {//if it's an acp then just update it
							
						newACP = iExternalClientAPI.queryACP(new DCIIUnitFilter(unitAddress));
							
						List<BaseTier> baseTiers = setupTiersList(dto.getTiers());//createTier(dto.getTiers().get(i));
						newACP.clearTiers();
						newACP.setTiers(baseTiers);
						newACP.setName(dto.getName() != null && !dto.getName().trim().isEmpty() ? dto.getName() : dto.getDecimalUnitAddress());
							
					}
					else {//if the acp is not found then the mad may have to be created first then the new acp created and attached to the mad
						forceRegen = true;	 
						BasePartition partition = defaultServices.getDefaultPartition(iExternalClientAPI);	 
						Affiliate affiliate = defaultServices.getDefaultAffiliate(iExternalClientAPI, partition);	 
						RatingRegion ratingRegion = defaultServices.getDefaultRatingRegion(iExternalClientAPI);	 
						Location location = defaultServices.getDefaultLocation(iExternalClientAPI, affiliate, partition, ratingRegion);	 
						BaseOperatorGroup operatorGroup = defaultServices.getDefaultOperatorGroup(iExternalClientAPI);	 
						BaseRegion region = defaultServices.getDefaultRegion(iExternalClientAPI, operatorGroup.getDBUID());	 
							
							
						if (newMAD == null) {//mad does not exist so create it 
							DecoderModel decoderModel = DecoderModel.getGenericModel(DecoderModelType.MD);
							newMAD = populateMAD(dto, partition, location, operatorGroup, decoderModel);
							newMAD = iExternalClientAPI.storeMAD(newMAD, true);
						}
							
						newACP = populateNewACP(unitAddress, dto.getName(), partition, newMAD, operatorGroup, region);//attach acp to mad
						
					    List<BaseTier> baseTiers = setupTiersList(dto.getTiers());//createTier(dto.getTiers().get(i));
						newACP.clearTiers();
						newACP.setTiers(baseTiers);
					}
					 
					iExternalClientAPI.storeACP(newACP, forceRegen);
				}//end for
			}//end acp list
		}
		finally {
			if (iExternalClientAPI != null) {
				BNCExternalAPIFactory.releaseExternalClientAPI(iExternalClientAPI);
			}
		}
	}
	
	private MAD populateMAD(HBOIrdDTO dto, BasePartition partition, Location location, BaseOperatorGroup OperatorGroup, DecoderModel decoderModel) {
		
    	MAD newMAD = MultiAcpIRD.getBlankMD();//assume we only work with MultiAcpIRD type of MD
	    
	    newMAD.setPT(partition.getPTNumber());
	    newMAD.setLocation(location);
	    newMAD.setOG(OperatorGroup);
	    newMAD.setSerialNumber(dto.getIRTSerialNumber());
	    newMAD.setUnitAddress(dto.getIRTUnitAddress());
	    newMAD.setName(dto.getIRTSerialNumber());
	    newMAD.setModel(decoderModel);

	    return newMAD;
	  }
	
	private ACP populateNewACP(String unitAddress, String name, BasePartition partition, MAD mad, BaseOperatorGroup OperatorGroup, BaseRegion region) {
		
	    ACP newACP = ACP.getBlank();
	    
	    newACP.setName(name != null && !name.trim().isEmpty() ? name : unitAddress);
	    newACP.setPT(partition.getPTNumber());
	    newACP.setMAD(mad);
	    newACP.setOG(OperatorGroup);
	    newACP.setRegion(region);
	    newACP.setAnchorUnitAddress(mad.getUnitAddress());
	    newACP.setUnitAddress(unitAddress);

	    newACP.setTierOn(true);
	    
	    return newACP;
	  }

	  protected List<BaseTier> setupTiersList(List<HBOTierDTO> dtoTiers) {
			
		  List<BaseTier> baseTiers = new ArrayList<BaseTier>();
		  for (HBOTierDTO tierDTO : dtoTiers) {
			  
			  if (!tierDTO.getOperation().equalsIgnoreCase("delete")) {
				  BaseTier bncTier = new BaseTier(tierDTO.getNumber());
				  bncTier.setNumber(tierDTO.getNumber());
				  baseTiers.add(bncTier);
			  }
		  }
			 
		  return baseTiers;
	  }
	
//	private List<BaseTier> createTier(HBOTierDTO tierDTO)
//	{
//		List<BaseTier> baseTiers = new ArrayList<BaseTier>();
//		if (tierDTO.getNumber() >= 0) {
//			baseTiers = new ArrayList<BaseTier>();
//			BaseTier bncTier = new BaseTier(tierDTO.getNumber());
//			bncTier.setNumber(tierDTO.getNumber());
//			baseTiers.add(bncTier);
//		}
//		
//		return baseTiers;		
//	}	

}
