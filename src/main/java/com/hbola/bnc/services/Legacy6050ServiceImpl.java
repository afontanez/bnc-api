package com.hbola.bnc.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.hbola.bnc.dto.HBOIrdDTO;
import com.motorola.bnc.common.shared.structures.Channel;
import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderModelNumber;
import com.motorola.bnc.common.shared.structures.IRD;
import com.motorola.bnc.common.shared.structures.Transcoder;
import com.motorola.bnc.common.shared.structures.TranscoderDto;

@Service("6050")
public class Legacy6050ServiceImpl extends LegacyServicesImpl { 

	@Override
	public DecoderModel getDecoderModel() {
		return DecoderModel.getModel(DecoderModelNumber.DSR_6050);
	}	
	
//	@Override
//	public void populateNewIRDSpecifics(HBOIrdDTO dto, IRD newIRD) throws Exception {
//	
//	}		  
//	  
	@Override
	public void updateExsitingIRDSpecifics(HBOIrdDTO dto, IRD existingIRD) throws Exception {			
	    
		//only 1 transcoder so no loop
		List<Transcoder> transcoders = existingIRD.getTranscoders();
		Transcoder transcoder = transcoders.get(0);		
		try {						
			transcoder.setHdReEncoderData(setHdReEncoderData(dto.getServices().get(0).getHdEnable()));
		}
		catch (Exception e) {
			transcoder.setHdReEncoderData(setHdReEncoderData(false));
		}
	}
	
	@Override
	protected List<Transcoder> setupTranscoders(HBOIrdDTO dto, Collection<Channel> allVirtualChannels) {//inputs are not used but are needed for override
		  
		List<Transcoder> transcoders = new ArrayList<Transcoder>();
		  	
		//only 1 transcoder so no loop
		Transcoder blankTranscoder = TranscoderDto.getBlank();
		blankTranscoder.setTranscoderIndex(1);
				
		try {
			blankTranscoder.setHdReEncoderData(setHdReEncoderData(dto.getServices().get(0).getHdEnable()));
		}
		catch (Exception e) {
			blankTranscoder.setHdReEncoderData(setHdReEncoderData(false));
		}
		transcoders.add(blankTranscoder);			
			
		return transcoders;
			
	}
}
