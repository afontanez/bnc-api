package com.hbola.bnc.services;

import com.hbola.bnc.dto.HBOIrdDTO;
import com.motorola.bnc.common.shared.structures.DecoderModel;

public interface LegacyServicesI {

	DecoderModel getDecoderModel();
	void tripIRD(String irdUnitAddress) throws Exception;
	void storeIRD(HBOIrdDTO dto) throws Exception;	
}