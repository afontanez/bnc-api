package com.hbola.bnc.services;

import org.springframework.stereotype.Service;

import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderModelNumber;

@Service("6401")
public class Legacy6401ServiceImpl extends Legacy64xxServiceImpl {
	
	@Override
	public DecoderModel getDecoderModel() {
		return DecoderModel.getModel(DecoderModelNumber.DSR_6401);
	}
	

}
