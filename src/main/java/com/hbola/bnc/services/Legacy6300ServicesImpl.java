package com.hbola.bnc.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.hbola.bnc.dto.HBOIrdDTO;
import com.hbola.bnc.dto.HBOTierDTO;
import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderModelNumber;
import com.motorola.bnc.common.shared.structures.IRD;
import com.motorola.refarch.infrastruct.gendata.BaseTier;

@Service("6300")
public class Legacy6300ServicesImpl extends LegacyServicesImpl {

	@Override
	public DecoderModel getDecoderModel() {
		return DecoderModel.getModel(DecoderModelNumber.DSR_6300);
	}
	
	@Override
	protected void populateNewIRDSpecifics(HBOIrdDTO dto, IRD newIRD) throws Exception {

		try {
			Integer templateId = dto.getTiers().get(0).getTemplateId();
			newIRD.setGeminiTemplateID(templateId);
		}
		catch (Exception e) {
			newIRD.setGeminiTemplateID(0);
		}
	}	
	
	@Override
	protected void updateExsitingIRDSpecifics(HBOIrdDTO dto, IRD existingIRD) throws Exception {
		
		try {
			Integer templateId = dto.getTiers().get(0).getTemplateId();
			existingIRD.setGeminiTemplateID(templateId);
		}
		catch (Exception e) {
			existingIRD.setGeminiTemplateID(0);
		}		
	}
	
	@Override
	protected List<BaseTier> setupTiersList(List<HBOTierDTO> dtoTiers) {
		 
		List<BaseTier> tiers = new ArrayList<BaseTier>();
		 
		try {

			if (!dtoTiers.get(0).getOperation().equalsIgnoreCase("delete")) {
				int[] iTiers = (int[])null;
				 
				//6300 only has 1 tier and service
				HBOTierDTO tierDTO = dtoTiers.get(0);
				Integer tierNumber = tierDTO.getNumber();
				boolean templateTierExtended = tierDTO.getIsTemplateTierExtended()==null?false:tierDTO.getIsTemplateTierExtended()==1;
				 
				iTiers = getArrayOfTiers(tierNumber, templateTierExtended);
				for (int i = 0; i < iTiers.length; i++) {
					BaseTier tier = new BaseTier(iTiers[i]);
					tier.setNumber(iTiers[i]);
					tiers.add(tier);
				}
			}
		}
		catch (Exception e) {
			//if something goes wrong with this monstrosity then just send back an empty tiers list.
		}
	      
		return tiers;
	}

	 /**
	  * Lift and shift from legacy code.
	  * 
	  * Be ye weary all who tread here as only God and the original programmer know what is actually happening....
	  */
	public static int[] getArrayOfTiers(int tierNumber, boolean templateTierExtended) {
		 
		 int[] iTiers = new int[3];
		 boolean IsFutureUse = false;

		 String strTierNumber = Integer.toString(tierNumber);
		 if (!templateTierExtended) {
			 int pos;
			 if (Integer.parseInt(strTierNumber.substring(strTierNumber.length() - 3)) == 400) {
				 IsFutureUse = true;
				 pos = strTierNumber.length() - 3;
				 iTiers[2] = Integer.parseInt(strTierNumber.substring(pos));

				 if (Integer.parseInt(strTierNumber.substring(strTierNumber.length() - 6, strTierNumber.length() - 3)) == 400) {
					 IsFutureUse = true;
					 pos = strTierNumber.length() - 6;
					 iTiers[1] = Integer.parseInt(strTierNumber.substring(pos, strTierNumber.length() - 3));

					 if (strTierNumber.length() == 7) {
						 pos--;
						 iTiers[0] = Integer.parseInt(strTierNumber.substring(pos, 1));
					 }
					 else if (strTierNumber.length() == 8) {
						 pos -= 2;
						 iTiers[0] = Integer.parseInt(strTierNumber.substring(pos, 2));
					 }
					 return iTiers;
				 }
			 }
			 else {
				 pos = strTierNumber.length() - 2;
				 iTiers[2] = Integer.parseInt(strTierNumber.substring(pos));
			 }
			 int prePos = pos;
			 pos -= 2;
			 iTiers[1] = Integer.parseInt(strTierNumber.substring(pos, prePos));
			 prePos = pos;
			 if ((strTierNumber.length() >= 7) || ((strTierNumber.length() == 6) && (!IsFutureUse))) {
				 pos -= 2;
				 iTiers[0] = Integer.parseInt(strTierNumber.substring(pos, prePos));
			 }
			 else {
				 pos--;
				 iTiers[0] = Integer.parseInt(strTierNumber.substring(pos, prePos));
			 }
		 }
		 else {
			 int index = 0;
			 strTierNumber = prependZeros(strTierNumber);
			 while (index < strTierNumber.length()) {
				 if (index == 0) {
					 iTiers[0] = Integer.parseInt(strTierNumber.substring(index, Math.min(index + 3, strTierNumber.length())));
				 }
				 else if (index == 3) {
					 iTiers[1] = Integer.parseInt(strTierNumber.substring(index, Math.min(index + 3, strTierNumber.length())));
				 }
				 else if (index == 6) {
					 iTiers[2] = Integer.parseInt(strTierNumber.substring(index, Math.min(index + 3, strTierNumber.length())));
				 }
				 index += 3;
			 }				 
		 }
		 
		 return iTiers;
	 }

	 private static String prependZeros(String tierNumber) {
		 int tierLength = tierNumber.length();

		 if (tierLength == 9) {
			 return tierNumber;
		 }
		 
		 int zerosCount = 9 - tierLength;
		 for (int i = 0; i < zerosCount; i++) {
			 tierNumber = "0" + tierNumber;
		 }
		 
		 return tierNumber;
	 }

}

