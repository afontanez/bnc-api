package com.hbola.bnc.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.hbola.bnc.dto.HBOIrdDTO;
import com.hbola.bnc.dto.HBOTierDTO;
import com.motorola.bnc.common.shared.structures.Affiliate;
import com.motorola.bnc.common.shared.structures.AutoHdSdAspectRatioConversionMethodEnumType;
import com.motorola.bnc.common.shared.structures.Channel;
import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderModelNumber;
import com.motorola.bnc.common.shared.structures.DecoderModelType;
import com.motorola.bnc.common.shared.structures.DecoderPreset;
import com.motorola.bnc.common.shared.structures.DefaultSdAspectRatioEnumType;
import com.motorola.bnc.common.shared.structures.HdOutputVideoResolutionFrameRateEnumType;
import com.motorola.bnc.common.shared.structures.HdReEncoderData;
import com.motorola.bnc.common.shared.structures.IRD;
import com.motorola.bnc.common.shared.structures.Location;
import com.motorola.bnc.common.shared.structures.Mpeg2SdOutputVideoResolutionEnumType;
import com.motorola.bnc.common.shared.structures.RatingRegion;
import com.motorola.bnc.common.shared.structures.SDIOutputControlType;
import com.motorola.bnc.common.shared.structures.SdReEncoderData;
import com.motorola.bnc.common.shared.structures.Transcoder;
import com.motorola.bnc.common.shared.structures.TranscoderDto;
import com.motorola.bnc.proxy.external.BNCExternalAPIFactory;
import com.motorola.bnc.proxy.external.IExternalClientAPI;
import com.motorola.refarch.infrastruct.gendata.BaseOperatorGroup;
import com.motorola.refarch.infrastruct.gendata.BasePartition;
import com.motorola.refarch.infrastruct.gendata.BaseRegion;
import com.motorola.refarch.infrastruct.gendata.BaseTier;
import com.motorola.refarch.shared.auth.DCIIUnitFilter;


public abstract class LegacyServicesImpl implements LegacyServicesI {
	
	@Autowired
	DefaultServicesI defaultServices;
	
	@Autowired
	ServicesI services;
	
	@Override
	public void tripIRD(String irdUnitAddress) throws Exception {
		
		IExternalClientAPI iExternalClientAPI = defaultServices.getBncConnection();
		DCIIUnitFilter unitFilter = new DCIIUnitFilter(irdUnitAddress);
			    
		iExternalClientAPI.tripIRD(unitFilter);
	}
	
	/* (non-Javadoc)
	 * @see com.hbola.bnc.services.LegacyServicesI#createDSR4201(com.hbola.bnc.dto.HBOIrdDTO)
	 */
	@Override
	public void storeIRD(HBOIrdDTO dto) throws Exception {
		
		IExternalClientAPI iExternalClientAPI = null;

		 boolean forceRegen = false;
		 IRD newIRD = IRD.getBlank();
		 try {
			 
			 iExternalClientAPI = defaultServices.getBncConnection();
			 DCIIUnitFilter dCIIUnitFilter = new DCIIUnitFilter(dto.getDecimalUnitAddress());
			 if (iExternalClientAPI.irdExists(dCIIUnitFilter)) {
				 
				 newIRD = iExternalClientAPI.queryIRD(dCIIUnitFilter);
				 if (newIRD.getModel().getKey().matches("N/A")) {
	       	
					 DecoderModel decoderModel =  getDecoderModel();	 
					 newIRD.setModel(decoderModel);
				 }
				 newIRD = updateExistingIRD(dto, newIRD);
			 }
			 else {
				 forceRegen = true;	 
				 BasePartition partition = defaultServices.getDefaultPartition(iExternalClientAPI);	 
				 Affiliate affiliate = defaultServices.getDefaultAffiliate(iExternalClientAPI, partition);	 
				 RatingRegion ratingRegion = defaultServices.getDefaultRatingRegion(iExternalClientAPI);	 
				 Location location = defaultServices.getDefaultLocation(iExternalClientAPI, affiliate, partition, ratingRegion);	 
				 BaseOperatorGroup operatorGroup = defaultServices.getDefaultOperatorGroup(iExternalClientAPI);	 
				 BaseRegion region = defaultServices.getDefaultRegion(iExternalClientAPI, operatorGroup.getDBUID());	 
				 DecoderModel decoderModel = getDecoderModel();
	    
				 newIRD = populateNewIRD(dto, partition, affiliate, location, operatorGroup, region, ratingRegion, decoderModel);	
			 }
			 
			 iExternalClientAPI.storeIRD(newIRD, forceRegen);			 
		}
		finally {
			if (iExternalClientAPI != null) {
				BNCExternalAPIFactory.releaseExternalClientAPI(iExternalClientAPI);
			}
		}
	}
	
	// populate new ird
	private IRD populateNewIRD(HBOIrdDTO dto, BasePartition partition, Affiliate affiliate, Location location, BaseOperatorGroup OperatorGroup, BaseRegion region, RatingRegion ratingRegion, DecoderModel decoderModel) throws Exception {
		
		IRD newIRD = IRD.getBlank();

	    newIRD.setPTID(partition.getPTNumber());	  
	    newIRD.setLocation(location);
	    newIRD.setOG(OperatorGroup);
	    newIRD.setRegion(region);
	    newIRD.setRatingRegion(ratingRegion);
	    newIRD.setUnitAddress(dto.getDecimalUnitAddress());
	    newIRD.setModel(decoderModel);
	    newIRD.setGeminiTemplateID(0);
	    newIRD.setName(dto.getName());
	    newIRD.setSerialNumber(dto.getName());
	    newIRD.setTimeZone((byte)17);//EASTERN_STANDARD_TIME

	    List<BaseTier> baseTiers = setupTiersList(dto.getTiers());
	    newIRD.clearTiers(); 
	    newIRD.setTiers(baseTiers);

	    //boolean hdStatMuxGroupEnabled = getHdStatMuxGroupEnabled(dto.getServices());	    
	    List<Transcoder> transcoders = setupTranscoders(dto, services.getAllChannels());//, hdStatMuxGroupEnabled);
	    newIRD.getTranscoders().clear();
	    newIRD.getTranscoders().addAll(transcoders);								 
	    
	    int presetId = findPreset(dto.getPresetId(), newIRD.getModel().getModelType(), newIRD.getModel().getModelNumber());
	    newIRD.setLastAppliedPreset(presetId);
	    
		if (dto.getTransportStreamOutput() != null) newIRD.setTransportStreamOutput((byte)dto.getTransportStreamOutput().intValue());//0(transport locked off) / 1(user controlled) / 2(PID alias locked on) / 3(PID alias locked off)		
		if (dto.getFrontPanelEnabled() != null) newIRD.setFrontPanelEnabled(dto.getFrontPanelEnabled().booleanValue());
		if (dto.getControlPortEnabled() != null) newIRD.setControlPortEnabled(dto.getControlPortEnabled());
				
		SDIOutputControlType sdiOutputControlType = mapSdiOutput(dto.getSdiOutput());
		newIRD.setSDIOutputControl(sdiOutputControlType);
	    
		newIRD.setEMMProviderID(0);
		newIRD.setEMMStreamRelease((byte)0);
		
	    populateNewIRDSpecifics(dto, newIRD);
	    
	    return newIRD;
	}
  	
	//update existing IRD
	private IRD updateExistingIRD(HBOIrdDTO dto, IRD existingIRD) throws Exception {
		  
		existingIRD.setGeminiTemplateID(0);
		existingIRD.setSerialNumber(dto.getName());

		List<BaseTier> baseTiers = setupTiersList(dto.getTiers());
		existingIRD.clearTiers(); 
		existingIRD.setTiers(baseTiers);
	      
		if (dto.getTransportStreamOutput() != null) existingIRD.setTransportStreamOutput((byte)dto.getTransportStreamOutput().intValue());//0(transport locked off) / 1(user controlled) / 2(PID alias locked on) / 3(PID alias locked off)		
		if (dto.getFrontPanelEnabled() != null) existingIRD.setFrontPanelEnabled(dto.getFrontPanelEnabled().booleanValue());
		if (dto.getControlPortEnabled() != null) existingIRD.setControlPortEnabled(dto.getControlPortEnabled());
					
		SDIOutputControlType sdiOutputControlType = mapSdiOutput(dto.getSdiOutput());
		existingIRD.setSDIOutputControl(sdiOutputControlType);
	      
		updateExsitingIRDSpecifics(dto, existingIRD);
	      
		return existingIRD;
	  }
	  	
	private int findPreset(String presetIdString, DecoderModelType decoderModelType, DecoderModelNumber decoderModelNumber) {
		
		int presetId = 1;
		try {
			presetId = Integer.parseInt(presetIdString);
		}
		catch (Exception e) {
			
			try {//get default preset for this model from the BNC
				IExternalClientAPI iExternalClientAPI = defaultServices.getBncConnection();
				DecoderPreset decoderPreset = iExternalClientAPI.queryModelDefaultDecoderPreset(1, decoderModelType, decoderModelNumber);//first input is partition number
				presetId = decoderPreset.getThePresetID().getPresetNumber();	
			}
			catch (Exception ex) {
				presetId = 1;
			}
		}
		
		return presetId;
	}

	  private AutoHdSdAspectRatioConversionMethodEnumType mapAutoSdAspectRatioConversionMethod(int value) {
		  
		  AutoHdSdAspectRatioConversionMethodEnumType conversionMethod = AutoHdSdAspectRatioConversionMethodEnumType.AFD;//case 0 
		  switch (value) {
		  
			  case 1: {
				  conversionMethod = AutoHdSdAspectRatioConversionMethodEnumType.NONE;		  
				  break;
			  }
			  case 2: {
				  conversionMethod = AutoHdSdAspectRatioConversionMethodEnumType.AFD_WO_TIMEOUT;		  
				  break;
			  }		  		  
		  }
		  return conversionMethod;
	  }
	 
	  private DefaultSdAspectRatioEnumType mapDefaultSdAspectRatio(int value) {
		  
		  DefaultSdAspectRatioEnumType aspectRatio = DefaultSdAspectRatioEnumType.LETTER_BOX;//case 0
		  
		  switch (value) {
		  
			  case 1: {
				  aspectRatio = DefaultSdAspectRatioEnumType.CENTER_CUT;
				  break;
			  }
			  case 2: {
				  aspectRatio = DefaultSdAspectRatioEnumType.ANAMORPHIC_16_9;
				  break;
			  }
			  case 3: {
				  aspectRatio = DefaultSdAspectRatioEnumType.LETTERBOX_14_9;
				  break;
			  }
		  }
		  		
		  return aspectRatio;		  
	  }
	  
	  private static SDIOutputControlType mapSdiOutput(Integer sdiOutput) {
				
		  SDIOutputControlType sdiOutputControlType = SDIOutputControlType.IRD_USER_CONTROLLED;
		  if (sdiOutput != null) {
			  
			  //mapping from db TB_EMMProvider
			  switch (sdiOutput) {        				
			  	case 0: { sdiOutputControlType = SDIOutputControlType.IRD_USER_CONTROLLED; break;}
				case 1: { sdiOutputControlType = SDIOutputControlType.SD_ONLY; break;}
				case 2: { sdiOutputControlType = SDIOutputControlType.NATIVE; break;}
				case 3: { sdiOutputControlType = SDIOutputControlType.DISABLED; break;}
			  }        
			}	  
			return sdiOutputControlType;
	  }

	  //overridden in 6300
	  protected List<BaseTier> setupTiersList(List<HBOTierDTO> dtoTiers) {
			
		  List<BaseTier> baseTiers = new ArrayList<BaseTier>();
		  for (HBOTierDTO tierDTO : dtoTiers) {
			  
			  if (!tierDTO.getOperation().equalsIgnoreCase("delete")) {
				  BaseTier bncTier = new BaseTier(tierDTO.getNumber());
				  bncTier.setNumber(tierDTO.getNumber());
				  baseTiers.add(bncTier);
			  }
		  }
			 
		  return baseTiers;
	  }
	  
	  //overridden in 6050 and 64xx 
	  protected List<Transcoder> setupTranscoders(HBOIrdDTO dto, Collection<Channel> allVirtualChannels) {
		  
		  List<Transcoder> transcoders = new ArrayList<Transcoder>();
		  	
		  // NEEDED FOR CONVERSION TO BNC 3.8
		  Transcoder transcoder1 = TranscoderDto.getBlank();
		  transcoder1.setTranscoderIndex(1);								
		  transcoders.add(transcoder1);
			
		  //6300 needs so to make the code more generic i just add 3 blank transcoders to all models
		  Transcoder transcoder2 = TranscoderDto.getBlank();
		  transcoder2.setTranscoderIndex(2);
		  transcoders.add(transcoder2);
			
		  Transcoder transcoder3 = TranscoderDto.getBlank();
		  transcoder3.setTranscoderIndex(3);
		  transcoders.add(transcoder3);
		
		  return transcoders;
	  }
	  	  
	  protected static int getNumTranscodersForDecoderModel(DecoderModel decoderModel) {
			  
		  int numTranscoders = 0;//4410 have no transcoders
		  if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6050) || decoderModel.isModelNumber(DecoderModelNumber.DSR_4460) || decoderModel.isModelNumber(DecoderModelNumber.DSR_4201)) numTranscoders = 1;
		  else if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6300)) numTranscoders = 3;
		  else if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6401)) numTranscoders = 1;
		  else if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6402)) numTranscoders = 2;
		  else if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6403)) numTranscoders = 3;
		  else if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6404)) numTranscoders = 4;
			  
		  return numTranscoders;
	  }
	  
	  protected static HdReEncoderData setHdReEncoderData(Boolean enabled) {
		    
		  HdReEncoderData hdReEncoderData = new HdReEncoderData();
		  
		  if (enabled == null || !enabled) {
			  hdReEncoderData.hdEnabled = 0;
		  }
		  else {
			  hdReEncoderData.hdEnabled = 1;
		  }		  
		  
		  hdReEncoderData.hdMinimumOutputVideoRateKbps = 15000;
		  hdReEncoderData.hdOutputVideoResolutionFrameRate = HdOutputVideoResolutionFrameRateEnumType.V1080_I_AUTO;
		    
		  return hdReEncoderData;
	  }

	  protected SdReEncoderData setSdReEncoderData(Boolean enabled, int sdConvertMethod, int sdConvertAspectRatio) {
		    
		  SdReEncoderData defaultSdReEncoderData = new SdReEncoderData();
		  
		  if (enabled == null || !enabled) {
			  defaultSdReEncoderData.sdEnabled = 0;  
		  }
		  else {
			  defaultSdReEncoderData.sdEnabled = 1;
		  }
		  
		  defaultSdReEncoderData.mpeg2SdOutputVideoRateKbps = 5000;
		  defaultSdReEncoderData.mpeg2SdOutputVideoResolution = Mpeg2SdOutputVideoResolutionEnumType.NTSC_528x480;		  

		  defaultSdReEncoderData.autoHdSdAspectRatioConversionMethod = mapAutoSdAspectRatioConversionMethod(sdConvertMethod);
		  defaultSdReEncoderData.defaultSdAspectRatio = mapDefaultSdAspectRatio(sdConvertAspectRatio);
		  
		  return defaultSdReEncoderData;
	  }	  
	  
	  //these can be overridden if needed
	  protected void populateNewIRDSpecifics(HBOIrdDTO dto, IRD newIRD) throws Exception {
	  }	
	  protected void updateExsitingIRDSpecifics(HBOIrdDTO dto, IRD existingIRD) throws Exception {		
	  }
	  
	  public abstract DecoderModel getDecoderModel();
}
