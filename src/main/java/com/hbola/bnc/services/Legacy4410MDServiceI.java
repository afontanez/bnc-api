package com.hbola.bnc.services;

import java.util.List;

import com.hbola.bnc.dto.HBOIrdDTO;

public interface Legacy4410MDServiceI {

	void tripIRD(String irdUnitAddress) throws Exception;
	void storeMD(List<HBOIrdDTO> dtoList) throws Exception;

}