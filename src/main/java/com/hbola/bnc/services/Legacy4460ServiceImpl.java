package com.hbola.bnc.services;

import org.springframework.stereotype.Service;

import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderModelNumber;

@Service("4460")
public class Legacy4460ServiceImpl extends LegacyServicesImpl {

	@Override
	public DecoderModel getDecoderModel() {
		return DecoderModel.getModel(DecoderModelNumber.DSR_4460);
	}
	
}
