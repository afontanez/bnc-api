package com.hbola.bnc.services;

public interface TokenServiceI {

	public boolean verify(String token) throws Exception;
}
