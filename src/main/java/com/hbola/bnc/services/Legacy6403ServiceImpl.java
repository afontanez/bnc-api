package com.hbola.bnc.services;

import org.springframework.stereotype.Service;

import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderModelNumber;

@Service("6403")
public class Legacy6403ServiceImpl extends Legacy64xxServiceImpl {

	@Override
	public DecoderModel getDecoderModel() {
		return DecoderModel.getModel(DecoderModelNumber.DSR_6403);
	}
}
