package com.hbola.bnc.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.hbola.bnc.config.ConfigCache;
import com.motorola.bnc.common.shared.structures.Affiliate;
import com.motorola.bnc.common.shared.structures.AffiliateGroup;
import com.motorola.bnc.common.shared.structures.Location;
import com.motorola.bnc.common.shared.structures.RatingRegion;
import com.motorola.bnc.proxy.external.BNCExternalAPIFactory;
import com.motorola.bnc.proxy.external.IExternalClientAPI;
import com.motorola.refarch.infrastruct.errdiag.MotStatusException;
import com.motorola.refarch.infrastruct.gendata.BaseOperatorGroup;
import com.motorola.refarch.infrastruct.gendata.BasePartition;
import com.motorola.refarch.infrastruct.gendata.BaseRegion;

@Service
public class DefaultServicesImpl implements DefaultServicesI {
	  
	/* (non-Javadoc)
	 * @see com.hbola.bnc.services.DefaultServicesI#getBncConnection()
	 */
	@Override
	public IExternalClientAPI getBncConnection() throws Exception {
		 //don't make these static field. static fields can not be reloaded unless on server restart. ConfigCache can reload at anytime if the values are changed.
		 String username = ConfigCache.INSTANCE.getProperties().getProperty("USERNAME");
		 String password = ConfigCache.INSTANCE.getProperties().getProperty("PASSWORD");
		 String serverIpAddress = ConfigCache.INSTANCE.getProperties().getProperty("SERVER_IP_ADDRESS");
		 
		 return BNCExternalAPIFactory.getExternalClientAPI(username, password, serverIpAddress);
	}
	
	
	/* (non-Javadoc)
	 * @see com.hbola.bnc.services.DefaultServicesI#getDefaultRegion(com.motorola.bnc.proxy.external.IExternalClientAPI, int)
	 */
	@Override
	public BaseRegion getDefaultRegion(IExternalClientAPI bncExternalClientAPI, int operatorGroupUID) throws MotStatusException
	{
		BaseRegion region = null;
		Collection<BaseRegion> regions = bncExternalClientAPI.queryAllRegions(operatorGroupUID);
		if ((regions != null) && (!regions.isEmpty())) {
			region = (BaseRegion)regions.iterator().next();
		}
		return region;
	}
		  
	/* (non-Javadoc)
	 * @see com.hbola.bnc.services.DefaultServicesI#getDefaultOperatorGroup(com.motorola.bnc.proxy.external.IExternalClientAPI)
	 */
	@Override
	public BaseOperatorGroup getDefaultOperatorGroup(IExternalClientAPI bncExternalClientAPI) throws MotStatusException
	{
		BaseOperatorGroup baseOperatorGroup = null;
		Collection<BaseOperatorGroup> operatorGroups = bncExternalClientAPI.queryAllOperatorGroups();
		if ((operatorGroups != null) && (!operatorGroups.isEmpty())) {
			baseOperatorGroup = operatorGroups.iterator().next();
		}
		return baseOperatorGroup;
	}
  
	/* (non-Javadoc)
	 * @see com.hbola.bnc.services.DefaultServicesI#getDefaultLocation(com.motorola.bnc.proxy.external.IExternalClientAPI, com.motorola.bnc.common.shared.structures.Affiliate, com.motorola.refarch.infrastruct.gendata.BasePartition, com.motorola.bnc.common.shared.structures.RatingRegion)
	 */
	@Override
	public Location getDefaultLocation(IExternalClientAPI bncExternalClientAPI, Affiliate defaultAffiliate, BasePartition defaultBasePartition, RatingRegion defaultRatingRegion) throws MotStatusException
	{
	Location location = null;
	Collection<Location> locations = bncExternalClientAPI.queryAllLocations(defaultAffiliate.getName());
	if ((locations != null) && (!locations.isEmpty())) {
	location = (Location)locations.iterator().next();
	
	}
	else {
	location = createDefaultLocation(bncExternalClientAPI, defaultBasePartition, defaultRatingRegion, defaultAffiliate);
	}
	
	return location; 
	}

	private Location createDefaultLocation(IExternalClientAPI bncExternalClientAPI, BasePartition defaultBasePartition, RatingRegion defaultRatingRegion, Affiliate defaultAffiliate) throws MotStatusException
	{
		Location foundLocation = bncExternalClientAPI.queryLocation(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_LOCATION_NAME"), ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_NAME"));
		if (foundLocation == null) {
			foundLocation = bncExternalClientAPI.storeLocation(getHboLagConstructedLocation(defaultBasePartition, defaultRatingRegion, defaultAffiliate));
		}
		return foundLocation;
	}

	private Location getHboLagConstructedLocation(BasePartition defaultBasePartition, RatingRegion defaultRatingRegion, Affiliate defaultAffiliate)
	{
		Location hbolagLocation = Location.getBlank();
		hbolagLocation.setName(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_LOCATION_NAME"));
		hbolagLocation.setContactName(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_CONTACT_NAME"));
		hbolagLocation.setAddress1(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_CONTACT_ADDRESS"));
		hbolagLocation.setCity(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_CONTACT_CITY"));
		hbolagLocation.setCountry(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_COUNTRY"));
		hbolagLocation.setPostalCode(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_ZIP"));
		hbolagLocation.setState(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_STATE"));
		hbolagLocation.setDBUID(0);
		hbolagLocation.setInDB(true);
		hbolagLocation.setAffiliate(defaultAffiliate);

		hbolagLocation.setTimeZone((byte)17);//EASTERN_STANDARD_TIME
		hbolagLocation.setRatingRegion(defaultRatingRegion);
		hbolagLocation.setPtNumber(defaultBasePartition.getPTNumber());
		
		return hbolagLocation;
	}
	  
	/* (non-Javadoc)
	 * @see com.hbola.bnc.services.DefaultServicesI#getDefaultRatingRegion(com.motorola.bnc.proxy.external.IExternalClientAPI)
	 */
	@Override
	public RatingRegion getDefaultRatingRegion(IExternalClientAPI bncExternalClientAPI) throws MotStatusException
	{

		RatingRegion ratingRegion = null;
		Collection<RatingRegion> ratingRegions = bncExternalClientAPI.queryAllRatingRegions();
		if ((ratingRegions != null) && (!ratingRegions.isEmpty())) {
			ratingRegion = ratingRegions.iterator().next();
		}
	
		return ratingRegion;	    
	}

	/* (non-Javadoc)
	 * @see com.hbola.bnc.services.DefaultServicesI#getDefaultPartition(com.motorola.bnc.proxy.external.IExternalClientAPI)
	 */
	@Override
	public BasePartition getDefaultPartition(IExternalClientAPI bncExternalClientAPI) throws Exception {

		BasePartition partition = null;
		Collection <BasePartition> partitions = bncExternalClientAPI.queryAllPartitions();
		if (partitions != null && !partitions.isEmpty()) {
			partition = (BasePartition) partitions.iterator().next();
		}

		return partition;
	}
		  
	/* (non-Javadoc)
	 * @see com.hbola.bnc.services.DefaultServicesI#getDefaultAffiliate(com.motorola.bnc.proxy.external.IExternalClientAPI, com.motorola.refarch.infrastruct.gendata.BasePartition)
	 */
	@Override
	public Affiliate getDefaultAffiliate(IExternalClientAPI bncExternalClientAPI, BasePartition defaultBasePartition) throws MotStatusException {

		Affiliate affiliate = null;
		Collection<Affiliate> affiliates = bncExternalClientAPI.queryAllAffiliates();
		if ((affiliates != null) && (!affiliates.isEmpty())) {
			affiliate = (Affiliate)affiliates.iterator().next();
		}
		else {
			affiliate = createDefaultAffiliate(bncExternalClientAPI, defaultBasePartition);
		}
	
		return affiliate; 
	}

	private Affiliate createDefaultAffiliate(IExternalClientAPI bncExternalClientAPI, BasePartition defaultBasePartition) throws MotStatusException {
	
		AffiliateGroup hbolagAffiliateGroup = null;		   		  
		Affiliate foundAffiliate = bncExternalClientAPI.queryAffiliate(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_NAME"));
		
		if (foundAffiliate == null) {
	
			hbolagAffiliateGroup = bncExternalClientAPI.queryAffiliateGroup(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_GROUP_NAME"));
			if (hbolagAffiliateGroup == null) {				  
				hbolagAffiliateGroup = bncExternalClientAPI.storeAffiliateGroup(getHboLagConstructedAffiliateGroup(defaultBasePartition));
			}
	
			foundAffiliate = bncExternalClientAPI.storeAffiliate(getHboLagConstructedAffiliate(hbolagAffiliateGroup, defaultBasePartition)); 
		}
	
		return foundAffiliate;
	}
			  
	private AffiliateGroup getHboLagConstructedAffiliateGroup(BasePartition defaultBasePartition) {
		AffiliateGroup hbolagAffiliateGroup = AffiliateGroup.getBlank();
		
		hbolagAffiliateGroup.setName(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_GROUP_NAME"));
		hbolagAffiliateGroup.setDBUID(0);
		hbolagAffiliateGroup.setInDB(true);
		hbolagAffiliateGroup.setPtNumber(defaultBasePartition.getPTNumber());
		
		return hbolagAffiliateGroup;
	}

	private Affiliate getHboLagConstructedAffiliate(AffiliateGroup hbolagAffiliateGroup, BasePartition defaultBasePartition) {
		List<AffiliateGroup> listAffiliateGroup = new ArrayList<AffiliateGroup>();
		
		listAffiliateGroup.add(hbolagAffiliateGroup);
		
		Affiliate hbolagAffiliate = Affiliate.getBlank();
		
		hbolagAffiliate.setName(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_NAME"));
		hbolagAffiliate.setAffiliateGroups(listAffiliateGroup);
		hbolagAffiliate.setContactName(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_CONTACT_NAME"));
		hbolagAffiliate.setAddress1(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_CONTACT_ADDRESS"));
		hbolagAffiliate.setCity(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_CONTACT_CITY"));
		hbolagAffiliate.setCountry(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_COUNTRY"));
		hbolagAffiliate.setPostalCode(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_ZIP"));
		hbolagAffiliate.setState(ConfigCache.INSTANCE.getProperties().getProperty("DEFAULT_AFFILIATE_STATE"));
		hbolagAffiliate.setDBUID(0);
		hbolagAffiliate.setInDB(true);
		hbolagAffiliate.setPtNumber(defaultBasePartition.getPTNumber());
		
		return hbolagAffiliate;
	}

}
