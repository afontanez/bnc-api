package com.hbola.bnc.services;

import java.util.Collection;
import java.util.List;

import com.hbola.bnc.dto.BNCDecoderDTO;
import com.hbola.bnc.dto.HBOIrdDTO;
import com.hbola.bnc.dto.HBOPresetDTO;
import com.motorola.bnc.common.shared.structures.Channel;

public interface ServicesI {
	
	void storeDSR74xxDSR4470(HBOIrdDTO dto) throws Exception;
	void deleteDSR74xxDSR4470(HBOIrdDTO dto) throws Exception;
	void tripDSR74xxDSR4470(String unitAddress) throws Exception;
	
	List<HBOPresetDTO> presets() throws Exception;	
	Collection<Channel> getAllChannels() throws Exception;
	List<BNCDecoderDTO> getAllMAD() throws Exception;
	List<BNCDecoderDTO> getAllIRD() throws Exception;
}