package com.hbola.bnc.services;

import java.util.List;

import com.hbola.bnc.dto.BNCDecoderDTO;
import com.hbola.bnc.dto.HBOPresetDTO;

public interface ServicesFacadeI {

	/**
	 * Creates the needed IRD on the BNC as described by the json parameter.
	 * 
	 * @param json
	 */
	void storeIRD(String json) throws Exception;

	/**
	 * Returns json of all the presets setup for decoderModel. Example of a vlid decoder model is DSR7412
	 * 
	 */
	List<HBOPresetDTO> presets() throws Exception;

	void tripIRD(String modelNumber, String unitAddress) throws Exception;

	List<BNCDecoderDTO> getAllBNCDecoders() throws Exception;	
}