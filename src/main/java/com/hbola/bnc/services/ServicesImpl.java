package com.hbola.bnc.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbola.bnc.dto.BNCDecoderDTO;
import com.hbola.bnc.dto.HBOIrdDTO;
import com.hbola.bnc.dto.HBOPresetDTO;
import com.hbola.bnc.dto.HBOTierDTO;
import com.hbola.bnc.dto.HBOTranscodersPreloadedDTO;
import com.hbola.bnc.utils.StringUtils;
import com.motorola.bnc.common.shared.structures.ACP;
import com.motorola.bnc.common.shared.structures.Affiliate;
import com.motorola.bnc.common.shared.structures.Channel;
import com.motorola.bnc.common.shared.structures.Decoder;
import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderPreset;
import com.motorola.bnc.common.shared.structures.IRD;
import com.motorola.bnc.common.shared.structures.IRDPort;
import com.motorola.bnc.common.shared.structures.Location;
import com.motorola.bnc.common.shared.structures.RatingRegion;
import com.motorola.bnc.common.shared.structures.Transcoder;
import com.motorola.bnc.common.shared.structures.TranscoderDto;
import com.motorola.bnc.proxy.external.BNCExternalAPIFactory;
import com.motorola.bnc.proxy.external.IExternalClientAPI;
import com.motorola.refarch.infrastruct.gendata.BasePartition;
import com.motorola.refarch.shared.auth.DCIIUnitFilter;

/**
 * In this class I will put the direct calls to the BNC as opposed to the
 * ServicsFacadeImpl where I will put more general services.
 * 
 * @author afontanez
 *
 */
@Service
public class ServicesImpl implements ServicesI {

	@Autowired
	DefaultServicesI defaultServices;

	private static int transactionId = 1; // must go from 1 to 999999 then roll
											// back to 1 as per arris document
											// 365-095-27560_x7

	private Integer getTransactionId() {
		if (transactionId >= 999999) {
			transactionId = 1;
		}
		return transactionId++;
	}

	@Override
	public void storeDSR74xxDSR4470(HBOIrdDTO dto) throws Exception {

		IExternalClientAPI iExternalClientAPI = null;
		try {
			iExternalClientAPI = defaultServices.getBncConnection();

			String unitConfigXml = StringUtils.getUnitConfiguration74xx4470(dto, getTransactionId());
			String unitConfigResponse = iExternalClientAPI.processIrdXml(unitConfigXml);

			List<Integer> BncTiers = StringUtils.parseUnitConfigResponse(unitConfigResponse);

			List<HBOTierDTO> tiersModified = StringUtils.compareTiers(BncTiers, dto.getTiers());

			dto.getTiers().clear();
			dto.setTiers(tiersModified);

			String xml = StringUtils.setupDSR74xxDSR4470XMLRequest(dto, getTransactionId());
			String response = iExternalClientAPI.processIrdXml(xml);

			if (response.toLowerCase().contains("failure")) {
				String message = StringUtils.extractMessageFromBNCResponse(response);
				throw new Exception(message);
			}
		}
		// don't catch exception - let it propagate up
		finally {
			if (iExternalClientAPI != null) {
				BNCExternalAPIFactory.releaseExternalClientAPI(iExternalClientAPI);
			}
		}

	}

	@Override
	public void tripDSR74xxDSR4470(String unitAddress) throws Exception {

		IExternalClientAPI iExternalClientAPI = null;
		try {

			iExternalClientAPI = defaultServices.getBncConnection();
			String xml = StringUtils.setupDSR74xxDSR4470XMLTripRequest(unitAddress, getTransactionId());

			String response = iExternalClientAPI.processIrdXml(xml);
			if (response.toLowerCase().contains("failure")) {
				String message = StringUtils.extractMessageFromBNCResponse(response);
				throw new Exception(message);
			}
		}
		// don't catch exception - let it propagate up
		finally {
			if (iExternalClientAPI != null) {
				BNCExternalAPIFactory.releaseExternalClientAPI(iExternalClientAPI);
			}
		}

	}

	@Override
	public List<HBOPresetDTO> presets() throws Exception {

		List<HBOPresetDTO> presetsList = new ArrayList<HBOPresetDTO>();
		IExternalClientAPI iExternalClientAPI = null;
		try {

			iExternalClientAPI = defaultServices.getBncConnection();

			SortedSet<DecoderModel> models = iExternalClientAPI.queryDecoderModels();

			BasePartition basePartition = defaultServices.getDefaultPartition(iExternalClientAPI);
			int ptNumber = basePartition.getPTNumber();
			SortedSet<DecoderPreset> set = iExternalClientAPI.queryDecoderPresets(ptNumber);

			set.forEach(decoderPreset -> {

				System.out.println(decoderPreset);

				Decoder decoder = decoderPreset.getTheDecoder();
				IRD ird = (IRD) decoder;

				ird.getTunedChannel();

				List<IRDPort> ports = ird.getPorts();

				Transcoder trans = TranscoderDto.getBlank();

				// if
				// (ird.getModel().isModelNumber(DecoderModelNumber.DSR_4470)) {
				// int i = 0;
				//
				// String info = decoderPreset.getInfo();
				// String key = decoderPreset.getKey();
				//
				//
				// i = 2;
				// }

				List<Transcoder> transcoders = ird.getTranscoders();

				List<HBOTranscodersPreloadedDTO> transcodersPreloadedList = new ArrayList<HBOTranscodersPreloadedDTO>();

				transcoders.forEach(transcoder -> {

					// boolean enabled = transcoder.isEnabled();
					// boolean tierON = transcoder.getTierOn();
					// boolean license = transcoder.isTranscoderLicenseInUse();
					// boolean uplink = transcoder.isUplinkBncInputControl();

					String vcNumber = "0";
					String vcTable = "0";
					if (transcoder.getVirtualChannel() != null) {
						vcNumber = Integer.toString(transcoder.getVirtualChannel().getNumber());
						vcTable = Integer.toString(transcoder.getVirtualChannel().getVCMID());
					}

					HBOTranscodersPreloadedDTO transcodersPreloaded = new HBOTranscodersPreloadedDTO(
							transcoder.getTranscoderIndex(), transcoder.getHdReEncoderData().hdEnabled == 1,
							transcoder.getSdReEncoderData().sdEnabled == 1, transcoder.getPassthruEnabled() == 1,
							vcNumber, vcTable, null);

					transcodersPreloadedList.add(transcodersPreloaded);
				});

				HBOPresetDTO presetDTO = new HBOPresetDTO();
				presetDTO.setName(decoderPreset.getThePresetName());
				presetDTO.setNumber(decoderPreset.getThePresetID().getPresetNumber());
				presetDTO.setTranscodersPreloaded(transcodersPreloadedList);

				String modelName = "N/A";
				if (ird.getModel() != null && ird.getModel().getModelNumber() != null) {
					modelName = ird.getModel().getModelNumber().toDbValue().toString();
				}
				presetDTO.setModelName(modelName);

				presetsList.add(presetDTO);
			});
		}
		// don't catch exception - let it propagate up
		finally {
			if (iExternalClientAPI != null) {
				BNCExternalAPIFactory.releaseExternalClientAPI(iExternalClientAPI);
			}
		}

		return presetsList;
	}

	@Override
	public Collection<Channel> getAllChannels() throws Exception {

		IExternalClientAPI iExternalClientAPI = null;
		Collection<Channel> channels = null;
		try {

			iExternalClientAPI = defaultServices.getBncConnection();
			channels = iExternalClientAPI.queryAllVirtualChannels();
		}
		// don't catch exception - let it propagate up
		finally {
			if (iExternalClientAPI != null) {
				BNCExternalAPIFactory.releaseExternalClientAPI(iExternalClientAPI);
			}
		}

		return channels;
	}

	@Override
	public List<BNCDecoderDTO> getAllIRD() throws Exception {

		final IExternalClientAPI iExternalClientAPI = defaultServices.getBncConnection();
		List<BNCDecoderDTO> uAs = new ArrayList<BNCDecoderDTO>();

		try {

			BasePartition defaultBasePartition = defaultServices.getDefaultPartition(iExternalClientAPI);
			Affiliate defaultAffiliate = defaultServices.getDefaultAffiliate(iExternalClientAPI, defaultBasePartition);
			RatingRegion defaultRatingRegion = defaultServices.getDefaultRatingRegion(iExternalClientAPI);
			Location defaultLocation = defaultServices.getDefaultLocation(iExternalClientAPI, defaultAffiliate,
					defaultBasePartition, defaultRatingRegion);

			Collection<DCIIUnitFilter> collection = iExternalClientAPI
					.queryAllIRDUnitAddresses(defaultLocation.getLName(), defaultAffiliate.getName());

			// 12629 total decoders - just getting the unitadress takes seconds;
			// getting anything else takes hours!!!
			Iterator<DCIIUnitFilter> iter = collection.iterator();
			while (iter.hasNext()) { // && uAs.size() < 100) {

				DCIIUnitFilter c = iter.next();

				try {
					IRD ird = iExternalClientAPI.queryIRD(c);
					String unitAddress = c.toDecimalString(false);
					String modelID = ird.getModel().toString();
					int[] tiers = ird.getTierNumbers();

					for (int i : tiers) {

						BNCDecoderDTO dto = new BNCDecoderDTO(unitAddress, i, "N/A", modelID, "Authorized", "N/A",
								ird.getInstallDate().getTime());
						uAs.add(dto);

						// for testing
						System.out.println("UAs list size: " + uAs.size() + " " + dto.print());
					}
				} catch (Exception e) {
				}
			}
		} finally {
			if (iExternalClientAPI != null) {
				BNCExternalAPIFactory.releaseExternalClientAPI(iExternalClientAPI);
			}
		}

		return uAs;
	}

	// MAD is actually the anchor of a 4410MD, MAD = Multiple ACP Decoder. ACP =
	// access control processor
	@Override
	public List<BNCDecoderDTO> getAllMAD() throws Exception {

		IExternalClientAPI iExternalClientAPI = null;
		List<BNCDecoderDTO> uAs = new ArrayList<BNCDecoderDTO>();

		try {

			iExternalClientAPI = defaultServices.getBncConnection();

			BasePartition defaultBasePartition = defaultServices.getDefaultPartition(iExternalClientAPI);
			Affiliate defaultAffiliate = defaultServices.getDefaultAffiliate(iExternalClientAPI, defaultBasePartition);
			RatingRegion defaultRatingRegion = defaultServices.getDefaultRatingRegion(iExternalClientAPI);
			Location defaultLocation = defaultServices.getDefaultLocation(iExternalClientAPI, defaultAffiliate,
					defaultBasePartition, defaultRatingRegion);

			Collection<DCIIUnitFilter> madCollection = iExternalClientAPI
					.queryAllMADUnitAddresses(defaultLocation.getName(), defaultAffiliate.getName(), null);// null
																											// means
																											// all
																											// types
																											// of
																											// MADs
																											// //DecoderModelType.MD);

			Iterator<DCIIUnitFilter> madIter = madCollection.iterator();
			while (madIter.hasNext()) {// && uAs.size() < 100) {

				DCIIUnitFilter madFilter = madIter.next();

				try {

					Collection<DCIIUnitFilter> acpsForMadCollection = iExternalClientAPI
							.queryAllACPUnitAddresses(madFilter);
					Iterator<DCIIUnitFilter> acpIter = acpsForMadCollection.iterator();
					while (acpIter.hasNext() && uAs.size() < 100) {

						DCIIUnitFilter acpFilter = acpIter.next();
						ACP acp = iExternalClientAPI.queryACP(acpFilter);

						String unitAddress = acpFilter.toDecimalString(false);
						int[] tiers = acp.getTierNumbers();
						Date installedDate = acp.getInstallDate();

						for (int i : tiers) {
							BNCDecoderDTO dto = new BNCDecoderDTO(unitAddress, i, "N/A", "DSR-4410MD", "Authorized",
									"N/A", installedDate.getTime());
							uAs.add(dto);
						}
					}
				} catch (Exception e) {
				}

			}
		} finally {
			if (iExternalClientAPI != null) {
				BNCExternalAPIFactory.releaseExternalClientAPI(iExternalClientAPI);
			}
		}

		return uAs;
	}

	@Override
	public void deleteDSR74xxDSR4470(HBOIrdDTO dto) throws Exception {
		// TODO Auto-generated method stub
		IExternalClientAPI iExternalClientAPI = null;
			
		try{
			iExternalClientAPI = defaultServices.getBncConnection();
			
			String xml = StringUtils.prepareDeleteIrd(dto);
			String result = iExternalClientAPI.processIrdXml(xml);
			
			if (result !=null && result.contains("failure")){
				throw new Exception("Deleting the unit Configuration is failed for unit address: "+dto.getDecimalUnitAddress());
			}
		}catch(Exception e){
			throw new Exception(e);
		}
		
	}

}
