package com.hbola.bnc.services;

import com.motorola.bnc.common.shared.structures.Affiliate;
import com.motorola.bnc.common.shared.structures.Location;
import com.motorola.bnc.common.shared.structures.RatingRegion;
import com.motorola.bnc.proxy.external.IExternalClientAPI;
import com.motorola.refarch.infrastruct.errdiag.MotStatusException;
import com.motorola.refarch.infrastruct.gendata.BaseOperatorGroup;
import com.motorola.refarch.infrastruct.gendata.BasePartition;
import com.motorola.refarch.infrastruct.gendata.BaseRegion;

public interface DefaultServicesI {

	IExternalClientAPI getBncConnection() throws Exception;

	BaseRegion getDefaultRegion(IExternalClientAPI bncExternalClientAPI, int operatorGroupUID) throws MotStatusException;

	BaseOperatorGroup getDefaultOperatorGroup(IExternalClientAPI bncExternalClientAPI) throws MotStatusException;

	Location getDefaultLocation(IExternalClientAPI bncExternalClientAPI, Affiliate defaultAffiliate, BasePartition defaultBasePartition, RatingRegion defaultRatingRegion) throws MotStatusException;

	RatingRegion getDefaultRatingRegion(IExternalClientAPI bncExternalClientAPI) throws MotStatusException;

	BasePartition getDefaultPartition(IExternalClientAPI bncExternalClientAPI) throws Exception;

	Affiliate getDefaultAffiliate(IExternalClientAPI bncExternalClientAPI, BasePartition defaultBasePartition) throws MotStatusException;

}