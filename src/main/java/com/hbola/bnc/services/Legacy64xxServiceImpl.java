package com.hbola.bnc.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.hbola.bnc.dto.HBOIrdDTO;
import com.hbola.bnc.dto.HBOServiceDTO;
import com.motorola.bnc.common.shared.structures.Channel;
import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.IRD;
import com.motorola.bnc.common.shared.structures.Transcoder;
import com.motorola.bnc.common.shared.structures.TranscoderDto;
import com.motorola.bnc.common.shared.structures.TranscoderHDStatMux;

public abstract class Legacy64xxServiceImpl extends LegacyServicesImpl {

	private boolean getHdStatMuxGroupEnabled(List<HBOServiceDTO> services) {

		int counter = 0;
		for (HBOServiceDTO service : services) {
			if (service.getHdEnable() != null && service.getHdEnable())
				counter++;
		}
		return counter >= 3;
	}

	private void populateCommon(HBOIrdDTO dto, IRD iRD) throws Exception {

		if (dto.getTransportStreamOutput() != null) {
			iRD.setTransportStreamOutput((byte) dto.getTransportStreamOutput().intValue());// 0(transport
																							// locked
																							// off)
																							// /
																							// 1(user
																							// controlled)
																							// /
																							// 2(PID
																							// alias
																							// locked
																							// on)
																							// /
																							// 3(PID
																							// alias
																							// locked
																							// off)
		}
		iRD.setInterfacePasswordReset((byte) 1);

		boolean hdStatMuxGroupEnabled = getHdStatMuxGroupEnabled(dto.getServices());
		iRD.setHdStatMuxGroupEnabled(hdStatMuxGroupEnabled);

		iRD.setHdStatMuxGroupBitRateKbps(35000);
		iRD.setMultiChannelRetuneEnabled(true);
	}

	private Channel getVirtualChannel(HBOServiceDTO serviceDTO, Collection<Channel> channels) {

		Channel virtualChannel = Channel.getBlank();

		Integer vcTable = Integer.parseInt(serviceDTO.getVcTable());
		Integer vcNumber = Integer.parseInt(serviceDTO.getVcNumber());

		if (vcTable == 0 || vcNumber == 0) {
			virtualChannel = channels.iterator().next(); // default channel
		} 
		else {
			for (Channel channel : channels) {

				try {

					if (channel.getVCMID() == vcTable && channel.getNumber() == vcNumber) {
						virtualChannel = channel;
					}
				} catch (Exception e) {
					virtualChannel = Channel.getBlank();
				}
			}
		}

		return virtualChannel;
	}

	private List<Transcoder> updateTranscoders(HBOIrdDTO dto, IRD existingIRD, Collection<Channel> allVirtualChannels) {

		List<Transcoder> transcoders = existingIRD.getTranscoders();
		for (HBOServiceDTO service : dto.getServices()) {

			Transcoder transcoder = transcoders.remove(service.getTranscoderID() - 1);

			transcoder.setHdReEncoderData(setHdReEncoderData(service.getHdEnable()));
			transcoder.setSdReEncoderData(setSdReEncoderData(service.getSdEnable(),
					service.getSdConvertMethod() == null ? 0 : service.getSdConvertMethod(),
					service.getSdConvertAspectRatio() == null ? 0 : service.getSdConvertAspectRatio()));
			transcoder.setPassthruEnabled(
					(byte) (service.getPassthruEnable() == null ? 0 : service.getPassthruEnable() ? 1 : 0));
			transcoder.setVirtualChannel(getVirtualChannel(service, allVirtualChannels));

			boolean hdStatMuxGroupEnabled = getHdStatMuxGroupEnabled(dto.getServices());
			transcoder.setTranscoderHDStatMux(setTranscoderHDStatMux(hdStatMuxGroupEnabled));
			transcoder.setUplinkBncInputControl(true);

			transcoders.add(service.getTranscoderID() - 1, transcoder);
		}

		return transcoders;

	}

	private static TranscoderHDStatMux setTranscoderHDStatMux(boolean statMuxGroupIncluded) {

		TranscoderHDStatMux transcoderHDStatMux = TranscoderHDStatMux.getBlank();

		transcoderHDStatMux.setStatMuxGroupIncluded(statMuxGroupIncluded);
		if (!statMuxGroupIncluded) {
			return transcoderHDStatMux;
		}

		transcoderHDStatMux.setStatMuxMaxBitRateKbps(18000);
		transcoderHDStatMux.setStatMuxMinBitRateKbps(5000);
		transcoderHDStatMux.setStatMuxWeight(5);

		return transcoderHDStatMux;
	}

	@Override
	public void populateNewIRDSpecifics(HBOIrdDTO dto, IRD newIRD) throws Exception {
		populateCommon(dto, newIRD);
	}

	@Override
	public void updateExsitingIRDSpecifics(HBOIrdDTO dto, IRD existingIRD) throws Exception {
		populateCommon(dto, existingIRD);
		updateTranscoders(dto, existingIRD, services.getAllChannels());
	}

	@Override
	protected List<Transcoder> setupTranscoders(HBOIrdDTO dto, Collection<Channel> allVirtualChannels) {

		List<Transcoder> transcoders = new ArrayList<Transcoder>();
		for (HBOServiceDTO service : dto.getServices()) {

			Transcoder transcoder = TranscoderDto.getBlank();
			transcoder.setTranscoderIndex(service.getTranscoderID());
			transcoder.setName(dto.getName() + "-" + service.getTranscoderID());

			transcoder.setHdReEncoderData(setHdReEncoderData(service.getHdEnable()));
			transcoder.setSdReEncoderData(setSdReEncoderData(service.getSdEnable(),
					service.getSdConvertMethod() == null ? 0 : service.getSdConvertMethod(),
					service.getSdConvertAspectRatio() == null ? 0 : service.getSdConvertAspectRatio()));
			transcoder.setPassthruEnabled(
					(byte) (service.getPassthruEnable() == null ? 0 : service.getPassthruEnable() ? 1 : 0));
			transcoder.setVirtualChannel(getVirtualChannel(service, allVirtualChannels));

			boolean hdStatMuxGroupEnabled = getHdStatMuxGroupEnabled(dto.getServices());
			transcoder.setTranscoderHDStatMux(setTranscoderHDStatMux(hdStatMuxGroupEnabled));
			transcoder.setUplinkBncInputControl(true);

			transcoders.add(transcoder);
		}

		// make sure we have setup the correct number of transcoders for this
		// decoder
		int x = transcoders.size();
		int numTranscoders = getNumTranscodersForDecoderModel(getDecoderModel());
		while (x++ < numTranscoders) {

			Transcoder blankTranscoder = TranscoderDto.getBlank();

			blankTranscoder.setTranscoderIndex(x);
			blankTranscoder.setName(dto.getName() + "-" + Integer.toString(x));

			Transcoder firstTranscoder = transcoders.get(0);
			blankTranscoder.setHdReEncoderData(setHdReEncoderData(false));
			blankTranscoder.setSdReEncoderData(setSdReEncoderData(false, 0, 0));
			blankTranscoder.setVirtualChannel(firstTranscoder.getVirtualChannel());
			blankTranscoder.setUplinkBncInputControl(false);

			transcoders.add(blankTranscoder);
		}

		return transcoders;

	}

	public abstract DecoderModel getDecoderModel();

}
