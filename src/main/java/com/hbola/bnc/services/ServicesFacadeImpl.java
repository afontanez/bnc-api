package com.hbola.bnc.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.hbola.bnc.dto.BNCDecoderDTO;
import com.hbola.bnc.dto.HBOIrdDTO;
import com.hbola.bnc.dto.HBOPresetDTO;
import com.hbola.bnc.utils.StringUtils;

/**
 * In this class I will put services that do not directly call the BNC. 
 * @author afontanez
 *
 */
@Service
public class ServicesFacadeImpl implements ServicesFacadeI {

	 //Thread safe as per Using Gson section of documentation: https://sites.google.com/site/gson/gson-user-guide#TOC-Using-Gson
	 private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().create();

	@Autowired
	private ServicesI services;
	
	@Autowired
	@Qualifier("4201")
	private LegacyServicesI legacy4201Services;

	@Autowired
	@Qualifier("4410MD")
	private Legacy4410MDServiceI legacy4410MDServices;
	
	@Autowired
	@Qualifier("4460")
	private LegacyServicesI legacy4460Services;
	
	@Autowired
	@Qualifier("6050")
	private LegacyServicesI legacy6050Services;
	
	@Autowired
	@Qualifier("6300")
	private LegacyServicesI legacy6300Services;

	@Autowired
	@Qualifier("6401")
	private LegacyServicesI legacy6401Services;
	
	@Autowired
	@Qualifier("6402")
	private LegacyServicesI legacy6402Services;

	@Autowired
	@Qualifier("6403")
	private LegacyServicesI legacy6403Services;

	@Autowired
	@Qualifier("6404")
	private LegacyServicesI legacy6404Services;

	
	//can't test with junit the connection to the BNC requires tomcat to be running.
	@Override
	public void storeIRD(String json) throws Exception {

		List<HBOIrdDTO> dtoList = new ArrayList<HBOIrdDTO>();
		//HBOIrdDTO dto = null;
		try {
			dtoList = GSON.fromJson(json, new TypeToken<List<HBOIrdDTO>>(){}.getType());
			//dto = GSON.fromJson(json, HBOIrdDTO.class);									
		}
		catch (Exception e) {//the exception message thrown by GSON is very cryptic so this text will help troubleshoot in the event of a GSON exception
			throw new Exception("Problem creating object from json: " + e.getMessage());
		}
			
		if (dtoList != null && !dtoList.isEmpty()) {
			HBOIrdDTO dto = dtoList.get(0);
			
			String modelNumber = StringUtils.validateAndFormat("Model Number", dto.getModelNumber());
			dto.setModelNumber(modelNumber);
				
			String decimalUnitAddress = StringUtils.validateAndFormat("DecimalUnitAddress", dto.getDecimalUnitAddress());
			dto.setDecimalUnitAddress(decimalUnitAddress);
				
			switch (dto.getModelNumber()) {
				case "DSR7412": {
					createOrEdit74xx(dto, 12);
					break;
				}
				case "DSR7409": {
					createOrEdit74xx(dto, 9);
					break;
				}
				case "DSR7406": {
					createOrEdit74xx(dto, 6);
					break;
				}				
				case "DSR7403": {
					createOrEdit74xx(dto, 3);
					break;
				}
				case "DSR7401": {								
					createOrEdit74xx(dto, 1);
					break;
				}
				case "DSR4470":	{
					dto.setDefaultNumberOfServices(1);
					services.storeDSR74xxDSR4470(dto);
					break;		
				}
				case "DSR6404": {
					legacy6404Services.storeIRD(dto);
					break;
				}
				case "DSR6403": {
					legacy6403Services.storeIRD(dto);
					break;
				}
				case "DSR6402": {
					legacy6402Services.storeIRD(dto);
					break;
				}
				case "DSR6300": {
					legacy6300Services.storeIRD(dto);
					break;
				}
				case "DSR6401": {
					legacy6401Services.storeIRD(dto);
					break;
				}
				case "DSR6050": {
					legacy6050Services.storeIRD(dto);
					break;
				}
				case "DSR4460": {
					legacy4460Services.storeIRD(dto);
					break;
				}
				case "DSR4410MD": {
					
					String iRTUnitAddress = StringUtils.validateAndFormat("IRTUnitAddress", dto.getIRTUnitAddress());
					dto.setIRTUnitAddress(iRTUnitAddress);
					
					legacy4410MDServices.storeMD(dtoList);//dto);
					break;					
				}
				case "DSR4201": {
					legacy4201Services.storeIRD(dto);
					break;
				}
				default: {
					throw new Exception("Invalid Model Number: " + dto.getModelNumber() + " . not handled by the bnc-api." );
				}
			}//end if dtoList is not empty or null
		}
	}

	private void createOrEdit74xx(HBOIrdDTO dto, int defaultNumberOfServices) throws Exception {
		String secondaryUnitAddress = StringUtils.validateAndFormat("Secondary Unit Address", dto.getSecondaryDecimalUnitAdress());
		dto.setSecondaryDecimalUnitAdress(secondaryUnitAddress);				
		dto.setDefaultNumberOfServices(defaultNumberOfServices);
		services.storeDSR74xxDSR4470(dto);
	}

	
	@Override
	public List<HBOPresetDTO> presets() throws Exception {
		
		return services.presets();
	}

	@Override
	public void tripIRD(String modelNumber, String unitAddress) throws Exception {
		
		modelNumber = StringUtils.validateAndFormat("Model Number", modelNumber);
		unitAddress = StringUtils.validateAndFormat("DecimalUnitAddress", unitAddress);
		
		switch (modelNumber) {
			case "DSR7412":
			case "DSR7409":
			case "DSR7406":
			case "DSR7403":
			case "DSR7401": 
			case "DSR4470":	{
				services.tripDSR74xxDSR4470(unitAddress);
				break;		
			}
			case "DSR6404": {
				legacy6404Services.tripIRD(unitAddress);
				break;
			}
			case "DSR6403": {
				legacy6403Services.tripIRD(unitAddress);
				break;
			}
			case "DSR6402": {
				legacy6402Services.tripIRD(unitAddress);
				break;
			}
			case "DSR6300": {
				legacy6300Services.tripIRD(unitAddress);
				break;
			}
			case "DSR6401": {
				legacy6401Services.tripIRD(unitAddress);
				break;
			}
			case "DSR6050": {
				legacy6050Services.tripIRD(unitAddress);
				break;
			}
			case "DSR4460": {
				legacy4460Services.tripIRD(unitAddress);
				break;
			}
			case "DSR4410MD": {
				legacy4410MDServices.tripIRD(unitAddress);
				break;
			}
			case "DSR4201": {
				legacy4201Services.tripIRD(unitAddress);
				break;
			}
			default: {
				throw new Exception("Invalid Model Number: " + modelNumber + " . not handled by the bnc-api." );
			}	
		}
	}
		
	@Override
	public List<BNCDecoderDTO> getAllBNCDecoders() throws Exception {
		
		List<BNCDecoderDTO> combined = new ArrayList<BNCDecoderDTO>();
			
		combined.addAll(services.getAllIRD());
		combined.addAll(services.getAllMAD());
		
		return combined;						
	}
	
}
