package com.hbola.bnc.controllers;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.hbola.bnc.services.Legacy6300ServicesImpl;
import com.hbola.bnc.services.ServicesFacadeI;
import com.hbola.bnc.utils.ApiResponse;
import com.hbola.bnc.utils.LoggerFormatter;


@EnableWebMvc
@RestController // this is used when we just want to return a service value
public class BNCController {

	private static final Logger logger = LogManager.getLogger(BNCController.class);
	
	@Autowired
	private ServicesFacadeI servicesFacade;
	 
	 @PostMapping(value = "/ird", produces = "application/json")
	 public ResponseEntity<ApiResponse> storeIRD(@RequestBody String json)  { //why not have the object created from json automatically from spring???
		 		 		 	 		 		 
		 logger.info(LoggerFormatter.logInfoStarted("/ird", json));
		 ApiResponse bncResponse = new ApiResponse(ApiResponse.OK, ApiResponse.NOOP);
		 try {			
			 servicesFacade.storeIRD(json);			 
			 bncResponse = new ApiResponse(ApiResponse.OK, ApiResponse.OK);			
		 }
		 catch(Exception e) {
			 String message = e.getMessage(); 
			 logger.error(LoggerFormatter.logError(message, json));			 
			 bncResponse = new ApiResponse(ApiResponse.ERROR, message);	
		 }		 
		 		 
		 logger.info(LoggerFormatter.logInfoEnded("/ird", json));
		 return ResponseEntity.status(HttpStatus.OK).body(bncResponse);
	 }
	
	 @RequestMapping(value = "/ird/trip/{modelNumber}/{unitAddress}", method = RequestMethod.GET, produces = "application/json")
	 public ResponseEntity<ApiResponse> trip(@PathVariable("modelNumber") String modelNumber, @PathVariable("unitAddress") String unitAddress)  {
		 
		 logger.info(LoggerFormatter.logInfoStarted("/ird/trip/{modelNumber}/{unitAddress}", "modelNumber: " + modelNumber + " unitAddress: " + unitAddress));
		 ApiResponse bncResponse = new ApiResponse(ApiResponse.OK, ApiResponse.NOOP);
		 try {			 
			 servicesFacade.tripIRD(modelNumber, unitAddress);			 
			 bncResponse = new ApiResponse(ApiResponse.OK, ApiResponse.OK);			 			
		 }
		 catch(Exception e) {
			 String message = e.getMessage(); 
			 logger.error(LoggerFormatter.logError(message, "modelNumber: " + modelNumber + " unitAddress: " + unitAddress));			 
			 bncResponse = new ApiResponse(ApiResponse.ERROR, message);			 	
		 }		 
		 
		 logger.info(LoggerFormatter.logInfoEnded("/ird/trip/{modelNumber}/{unitAddress}", "modelNumber: " + modelNumber + " unitAddress: " + unitAddress));
		 return ResponseEntity.status(HttpStatus.OK).body(bncResponse);
	 }
	
	 
	 @RequestMapping(value = "/presets", method = RequestMethod.GET, produces = "application/json")
	 public ResponseEntity<ApiResponse> presets()  { 
		 	
		 logger.info(LoggerFormatter.logInfoStarted("/presets", null));
		 ApiResponse bncResponse = new ApiResponse(ApiResponse.OK, ApiResponse.NOOP);
		 try {						 
			 bncResponse = new ApiResponse(ApiResponse.OK, servicesFacade.presets());
		 }
		 catch(Exception e) {
			 String message = e.getMessage(); 
			 logger.error(LoggerFormatter.logError(message, null));
			 bncResponse = new ApiResponse(ApiResponse.ERROR, message);			 						
		 }		 
		 
		 logger.info(LoggerFormatter.logInfoEnded("/presets", null));
		 return ResponseEntity.status(HttpStatus.OK).body(bncResponse);
	 }
	
	 
	 //@RequestMapping(value = "/bnc-decoders", method = RequestMethod.GET, produces = "application/json")
	 @GetMapping(value = "/bnc-decoders", produces = "application/json")
	 public ResponseEntity<ApiResponse> gtAllDecdoers()  { 
		 	
		 logger.info(LoggerFormatter.logInfoStarted("/getAllDecoders", null));
		 ApiResponse bncResponse = new ApiResponse(ApiResponse.OK, ApiResponse.NOOP);
		 try {						 
			 bncResponse = new ApiResponse(ApiResponse.OK, servicesFacade.getAllBNCDecoders());
		 }
		 catch(Exception e) {
			 String message = e.getMessage(); 
			 logger.error(LoggerFormatter.logError(message, null));
			 bncResponse = new ApiResponse(ApiResponse.ERROR, message);			 						
		 }		 
		 
		 logger.info(LoggerFormatter.logInfoEnded("/getAllDecoders", null));
		 return ResponseEntity.status(HttpStatus.OK).body(bncResponse);		 
	 }
	
	 @RequestMapping(value = "/parseExtendedTier/{tierNumber}/{templateTierExtended}", method = RequestMethod.GET, produces = "application/json")
	 public ResponseEntity<ApiResponse> parse6300Tiers(@PathVariable("tierNumber") Integer tierNumber, @PathVariable("templateTierExtended") Boolean templateTierExtended)  { 
		 	
		 logger.info(LoggerFormatter.logInfoStarted("/parse6300Tier", null));
		 ApiResponse bncResponse = new ApiResponse(ApiResponse.OK, ApiResponse.NOOP);
		 try {						 
			 bncResponse = new ApiResponse(ApiResponse.OK, Legacy6300ServicesImpl.getArrayOfTiers(tierNumber, templateTierExtended));
		 }
		 catch(Exception e) {
			 String message = e.getMessage(); 
			 logger.error(LoggerFormatter.logError(message, null));
			 bncResponse = new ApiResponse(ApiResponse.ERROR, message);			 						
		 }		 
		 
		 logger.info(LoggerFormatter.logInfoEnded("/parse6300Tier", null));
		 return ResponseEntity.status(HttpStatus.OK).body(bncResponse);		 
	 }
	
}
