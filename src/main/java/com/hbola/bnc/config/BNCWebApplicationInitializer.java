package com.hbola.bnc.config;




import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;


/**
 * This class is used for runtime spring configuration.
 * @author afontanez
 *
 */
public class BNCWebApplicationInitializer implements WebApplicationInitializer {	
	
	@Override
	public void onStartup(ServletContext container) throws ServletException {
		
		ConfigCache.INSTANCE.reload();
		
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.setConfigLocation("com.hbola.bnc");//scan top level package for anotations to create spring beans
	 
		container.addListener(new ContextLoaderListener(context));
	 
		ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher", new DispatcherServlet(context));
	         
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");
	}

}
