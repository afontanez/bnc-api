package com.hbola.bnc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.hbola.das.interceptor.IncomingApiCallInterceptor;

@Configuration
public class BNCAPIConfig extends WebMvcConfigurationSupport {

	@Bean
	public IncomingApiCallInterceptor httpInterceptor() {
		return new IncomingApiCallInterceptor();
	}
		
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(httpInterceptor());
	}
}
