package com.hbola.bnc.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hbola.bnc.utils.LoggerFormatter;
import com.motorola.refarch.infrastruct.errdiag.StatusMsgImpl;

/**
 * This class is used to access and setup resources that are at the tomcat level and not at the web app level. 
 * 
 * @author afontanez
 *
 */
public enum ConfigCache {
	
	INSTANCE;//all calls must be made through instance to ensure singleton use
	
	public static final Logger logger = LogManager.getFormatterLogger(ConfigCache.class);
	
	private static final String TOMCAT_HOME_DEFAULT = "C:/Program Files/Apache Software Foundation/Tomcat 7.0";
    private static final Properties PROPS = new Properties();

    public Properties getProperties() {
        return PROPS;
    }
    
    public void reload() {       

    	String tomcatHome = tomcatHome();
    	
    	logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    	logger.info("ConfigCache reload. tomcatHome: " + tomcatHome);
    	logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    	
        try {
        	
        	File file = new File(tomcatHome + "/bnc-api.properties");           
            InputStream fis = new FileInputStream(file);            
            PROPS.load(fis);
                        
            String statusMsgsPropertiesPath = tomcatHome + "/StatusMsgs.properties";
            StatusMsgImpl.readProperties(statusMsgsPropertiesPath);
                       
        } catch (IOException e) {
            String message = LoggerFormatter.logError("Problem loading bnc-api.properties: " + e.getMessage(), null);
            logger.error(message);
        }
    }
       
    private static String tomcatHome() {
    	
    	String tomcatHome = null;
    	try {
    		RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
    		Map<String, String> map = bean.getSystemProperties();
    		tomcatHome = map.get("catalina.home");
    	 
    		if (tomcatHome == null || tomcatHome.isEmpty()) {    			
    			String message = LoggerFormatter.logError("catalina.home environment variable is null. Defaulting to " + TOMCAT_HOME_DEFAULT, null);
    			logger.error(message);
    			tomcatHome = TOMCAT_HOME_DEFAULT;
    		}
    	}
    	catch (Exception e) {
    		String message = LoggerFormatter.logError("Exception thrown when finding catalina.home. Defaulting to " + TOMCAT_HOME_DEFAULT, null);
    		logger.error(message);
    		tomcatHome = TOMCAT_HOME_DEFAULT;
    	}
    	 
    	return tomcatHome;
    }
    
}
