

package com.hbola.das.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.hbola.bnc.services.TokenServiceI;

@ComponentScan
public class IncomingApiCallInterceptor extends HandlerInterceptorAdapter {

	@Autowired 
	private TokenServiceI tokenService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {	
		
		boolean isValidRequest = false;
		try{
			String token = request.getHeader("Authorization");
	        if(tokenService.verify(token)) {
	        	response.setStatus(HttpStatus.I_AM_A_TEAPOT.value());
	            isValidRequest = true;
	        }
	        else {
	            response.setStatus(HttpStatus.UNAUTHORIZED.value());
	        }
		}catch(Exception e){
            response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		
		return isValidRequest;
	}
	
}
